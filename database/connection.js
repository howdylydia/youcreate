import Sequelize from "sequelize";
import config from "./config/config.mjs";

let sequelize;
if (process.env.NODE_ENV === "production") {
	sequelize = new Sequelize(config.production);
} else {
	sequelize = new Sequelize(config.development);
	
}

if (sequelize) {
	sequelize.authenticate()
		.then(() => {
			console.log('Connection has been established successfully....');
		})
		.catch(err => {
			console.log('Unable to connect to the database:' + err);
		});
}
// try {
// 	await sequelize.authenticate();
// 	console.log('Connection has been established successfully.');
// } catch (error) {
// 	console.error('Unable to connect to the database:', error);
// }

const connection = sequelize;

export default connection;
