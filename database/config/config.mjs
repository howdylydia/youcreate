export default {
	development: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME_DEVELOPMENT,
		host: process.env.DB_HOSTNAME,
		port: process.env.DB_PORT,
		dialect: "mysql",
		// dialectOptions: {
		// 	ssl: {
		// 	rejectUnauthorized: true,
		// 	}
		// },
		dialectModule: require('mysql2')
		// ssl: {
		// 	key: cKey,
		// 	cert: cCert,
		// 	ca: cCA
		//   }
	},
	production: {
		username: process.env.DB_USERNAME_PROD,
		password: process.env.DB_PASSWORD_PROD,
		database: process.env.DB_NAME_PROD,
		host: process.env.DB_HOSTNAME_PROD,
		port: process.env.DB_PORT_PROD,
		logging: false,
		dialect: "mysql",
		dialectOptions: {
			ssl: {
			rejectUnauthorized: true,
			}
		},
		dialectModule: require('mysql2')
		// ssl: {
		// 	key: cKey,
		// 	cert: cCert,
		// 	ca: cCA
		// }
	},
};
