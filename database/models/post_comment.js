import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const initPost_Comment = (sequelize, Types) => {
	class Post_Comment extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	Post_Comment.init(
		{
			id: {
				type: Types.UUID,
				defaultValue: Types.UUIDV4,
				primaryKey: true,
			},
            comment: DataTypes.STRING,
			userId: {
				type: Types.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "users",
					key: "id",
					as: "userId",
				},
			},
			postId: {
				type: Types.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "posts",
					key: "id",
					as: "postId",
				},
			},
		},
		{
			sequelize,
			modelName: "Post_Comment",
			tableName: "post_comments",
			createdAt: "created_at",
			updatedAt: "updated_at",
		}
	);
	return Post_Comment;
};

export default initPost_Comment(connection, DataTypes);
