import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const initPost_Favourite = (sequelize, Types) => {
	class Post_Favourite extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	Post_Favourite.init(
		{
			id: {
				type: Types.UUID,
				defaultValue: Types.UUIDV4,
				primaryKey: true,
			},
			userId: {
				type: Types.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "users",
					key: "id",
					as: "userId",
				},
			},
			postId: {
				type: Types.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "posts",
					key: "id",
					as: "postId",
				},
			},
		},
		{
			sequelize,
			modelName: "Post_Favourite",
			tableName: "post_favourites",
			createdAt: "created_at",
			updatedAt: "updated_at",
		}
	);
	return Post_Favourite;
};

export default initPost_Favourite(connection, DataTypes);
