import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const initMembership = (sequelize, Types) => {
	class Membership extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	Membership.init(
		{
			id: {
				type: Types.UUID,
				defaultValue: Types.UUIDV4,
				primaryKey: true,
			},
			userId: {
				type: DataTypes.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "users",
					key: "id",
					as: "userId",
				},
			},
            membership_type: {
				type: DataTypes.ENUM,
				values: ["basic", "plus", "pro"],
			},
            payment_method: {
				type: DataTypes.ENUM,
				values: ["recurring", "lump-sum"],
			},
			payment_status: {
				type: DataTypes.ENUM,
				values: ["paid", "unpaid"],
			},
            start_at: DataTypes.DATE,
            end_at: DataTypes.DATE,
            total_paid: DataTypes.FLOAT
		},
		{
			sequelize,
			modelName: "Membership",
			tableName: "memberships",
			createdAt: "created_at",
			updatedAt: "updated_at",
		}
	);
	return Membership;
};

export default initMembership(connection, DataTypes);