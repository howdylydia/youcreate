import { Model, DataTypes } from 'sequelize';
import connection from "../connection";

const initRSVP = (sequelize, Types) => {
    class RSVP extends Model {
        /**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
            RSVP.belongsTo(models.User, { foreignKey: 'userId', as: 'user' });
            RSVP.belongsTo(models.Event, { foreignKey: 'eventId', as: 'event' });
		}
    }
    RSVP.init({
        id: {
            type: Types.UUID,
            defaultValue: Types.UUIDV4,
            primaryKey: true,
        },
        status: {
            type: DataTypes.ENUM("going", "not-going", "waitlist"),
            defaultValue: "going",
        },
        is_paid: DataTypes.BOOLEAN,
        eventId: {
            type: DataTypes.UUID,
            allowNull: false,
            onDelete: "CASCADE",
            references: {
                model: "events",
                key: "id",
                as: "eventId",
            },
        },
        userId: {
            type: DataTypes.UUID,
            allowNull: false,
            onDelete: "CASCADE",
            references: {
                model: "users",
                key: "id",
                as: "userId",
            },
        },
    },
    {
        sequelize,
        modelName: "RSVP",
        tableName: "rsvps",
        createdAt: "created_at",
        updatedAt: "updated_at",
      }
    );
    return RSVP;
};

export default initRSVP(connection, DataTypes);