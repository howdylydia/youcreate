import User from "./user";
import Category from "./category";
import Course from "./course";
import Post from "./post";
import Post_Favourite from "./post_favourite";
import Post_Comment from "./post_comment";
import Prompt from "./prompt";
import Video from "./video";
import Favourite from "./favourite";
import Enrolment from "./enrolment";
import Instructor_Earning from "./instructor_earning";
import Course_Progress from "./course_progress";
import Course_Asset from "./course_asset";
import Membership from "./membership";
import Event from "./event";
import RSVP from "./rsvp";
import Contest from './contest';

// // user can have many RSVPs
// User.hasMany(RSVP, { foreignKey: "userId", as:"rsvps" });
// RSVP.belongsTo(User, { foreignKey: "rsvp", as:"rsvps"});

// // event can have many RSVPs
// Event.hasMany(RSVP, { foreignKey: "eventid", as:"events" });
// RSVP.belongsTo(Event, { foreignKey: "rsvp", as:"rsvps"});

User.hasMany(Course, { foreignKey: "userId", as: "courses" });
Course.belongsTo(User, { foreignKey: "userId", as: "user" });

User.hasMany(Post, { foreignKey: "userId", as: "posts" });
Post.belongsTo(User, { foreignKey: "userId", as: "user" });

Category.hasMany(Course, { foreignKey: "catId", as: "courses" });
Course.belongsTo(Category, { foreignKey: "catId", as: "category" });

Category.hasMany(Post, { foreignKey: "catId", as: "posts" });
Post.belongsTo(Category, { foreignKey: "catId", as: "category" });

Category.hasMany(Prompt, { foreignKey: "catId", as: "prompts" });
Prompt.belongsTo(Category, { foreignKey: "catId", as: "category" });

Course.hasMany(Video, { foreignKey: "courseId", as: "videos" });
Video.belongsTo(Course, { foreignKey: "courseId", as: "course" });

Post.hasMany(Video, { foreignKey: "postId", as: "videos" });
Video.belongsTo(Post, { foreignKey: "postId", as: "post" });

User.hasMany(Video, { foreignKey: "userId", as: "videos" });
Video.belongsTo(User, { foreignKey: "userId", as: "user" });

User.hasMany(Favourite, { foreignKey: "userId", as: "favourites" });
Favourite.belongsTo(User, { foreignKey: "userId", as: "user" });

Course.hasMany(Favourite, { foreignKey: "courseId", as: "favourites" });
Favourite.belongsTo(Course, { foreignKey: "courseId", as: "course" });

User.hasMany(Post_Favourite, { foreignKey: "userId", as: "post_favourites" });
Post_Favourite.belongsTo(User, { foreignKey: "userId", as: "user" });

Post.hasMany(Post_Favourite, { foreignKey: "postId", as: "post_favourites" });
Post_Favourite.belongsTo(Post, { foreignKey: "postId", as: "post" });

User.hasMany(Post_Comment, { foreignKey: "userId", as: "post_comments" });
Post_Comment.belongsTo(User, { foreignKey: "userId", as: "user" });

Post.hasMany(Post_Comment, { foreignKey: "postId", as: "post_comments" });
Post_Comment.belongsTo(Post, { foreignKey: "postId", as: "post" });

User.hasMany(Enrolment, { foreignKey: "userId", as: "enrolments" });
Enrolment.belongsTo(User, { foreignKey: "userId", as: "user" });

Course.hasMany(Enrolment, { foreignKey: "courseId", as: "enrolments" });
Enrolment.belongsTo(Course, { foreignKey: "courseId", as: "course" });

User.hasMany(Instructor_Earning, {
	foreignKey: "userId",
	as: "instructor_earnings",
});
Instructor_Earning.belongsTo(User, { foreignKey: "userId", as: "user" });

Course.hasMany(Instructor_Earning, {
	foreignKey: "courseId",
	as: "instructor_earnings",
});
Instructor_Earning.belongsTo(Course, { foreignKey: "courseId", as: "course" });

User.hasMany(Course_Progress, {
	foreignKey: "userId",
	as: "course_progresses",
});
Course_Progress.belongsTo(User, { foreignKey: "userId", as: "user" });

Course.hasMany(Course_Progress, {
	foreignKey: "courseId",
	as: "course_progresses",
});
Course_Progress.belongsTo(Course, { foreignKey: "courseId", as: "course" });

Video.hasMany(Course_Progress, {
	foreignKey: "videoId",
	as: "course_progresses",
});
Course_Progress.belongsTo(Video, { foreignKey: "videoId", as: "video" });

Membership.belongsTo(User, { foreignKey: "userId", as: "user" });

User.hasMany(RSVP, { foreignKey: 'userId', as: 'rsvps' });
RSVP.belongsTo(User, { foreignKey: 'userId', as: 'user' });
RSVP.belongsTo(Event, { foreignKey: 'eventId', as: 'event' });


// Contest to User
// Contest belongs to User
Contest.belongsTo(User, { foreignKey: 'userId', as: 'creator' });
// User has many Contests
User.hasMany(Contest, { foreignKey: 'userId', as: 'contests' });

// Contest to Post 
// Each Post can be a submission to a Contest
Post.belongsTo(Contest, { foreignKey: 'contestId', as: 'contestSubmission' });

// A Contest has many Posts as submissions
Contest.hasMany(Post, { foreignKey: 'contestId', as: 'submissions' });

// Optional: if you want to directly associate the winning post with the contest
Contest.belongsTo(Post, { foreignKey: 'winnerPostId', as: 'winningSubmission' });

// Contest to Category association
// Contest belongs to a Category
Contest.belongsTo(Category, { foreignKey: 'catId', as: 'category' });

// A Category can have many Contests
Category.hasMany(Contest, { foreignKey: 'catId', as: 'contests' });



export {
	User,
	Course,
	Post,
	Post_Favourite,
	Post_Comment,
	Prompt,
	Category,
	Video,
	Favourite,
	Enrolment,
	Instructor_Earning,
	Course_Progress,
	Course_Asset,
	Membership,
	Event,
	RSVP,
	Contest
};
