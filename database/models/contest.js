import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const initContest = (sequelize, Types) => {
	class Contest extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here, for example:
			// this.belongsTo(models.User, { foreignKey: 'userId', as: 'user' });
			// this.hasMany(models.Post, { foreignKey: 'contestId', as: 'submissions' });
		}
	}
	Contest.init(
		{
			id: {
				type: Types.UUID,
				defaultValue: Types.UUIDV4,
				primaryKey: true,
			},
			title: DataTypes.STRING,
			slug: DataTypes.STRING,
            short_desc: DataTypes.TEXT,
			description: DataTypes.TEXT,
			requirements: DataTypes.TEXT,
			submit_link: DataTypes.TEXT,
			contest_start: DataTypes.DATE,
			contest_end: DataTypes.DATE,
			reward: DataTypes.TEXT,
			userId: {
				type: DataTypes.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "users",
					key: "id",
					as: "userId",
				},
			},
			winnerPostId: {
				type: DataTypes.UUID,
				onDelete: "SET NULL",
				references: {
					model: "posts",
					key: "id",
					as: "winnerPostId",
				},
				allowNull: true, // Initially null until a winner is decided
			},
			catId: {
				type: DataTypes.UUID,
				allowNull: true, // Optional, depending on whether contests are categorized
				onDelete: "CASCADE",
				references: {
					model: "categories",
					key: "id",
					as: "catId",
				},
			}
		},
		{
			sequelize,
			modelName: "Contest",
			tableName: "contests",
			createdAt: "created_at",
			updatedAt: "updated_at",
		}
	);
	return Contest;
};
export default initContest(connection, DataTypes);