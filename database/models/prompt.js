import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const initPrompt = (sequelize, Types) => {
	class Prompt extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	Prompt.init(
		{
			id: {
				type: Types.UUID,
				defaultValue: Types.UUIDV4,
				primaryKey: true,
			},
			title: DataTypes.STRING,
			slug: DataTypes.STRING,
			short_desc: DataTypes.TEXT,
            desc: DataTypes.TEXT,
			image: DataTypes.STRING,
			userId: {
				type: DataTypes.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "users",
					key: "id",
					as: "userId",
				},
			},
			catId: {
				type: DataTypes.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "categories",
					key: "id",
					as: "catId",
				},
			}
		},
		{
			sequelize,
			modelName: "Prompt",
			tableName: "prompts",
			createdAt: "created_at",
			updatedAt: "updated_at",
		}
	);
	return Prompt;
};
export default initPrompt(connection, DataTypes);
