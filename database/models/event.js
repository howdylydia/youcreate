import { Model, DataTypes } from 'sequelize';
import connection from '../connection';

const initEvent = (sequelize, Types) => {
    class Event extends Model {
        /**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
            // Association with RSVP model for attendees
            // Event.belongsToMany(models.User, {
            //     through: models.RSVP,
            //     foreignKey: 'eventId',
            //     as: 'attendees',
            // });
		}
    }
    Event.init({
        id: {
            type: Types.UUID,
            defaultValue: Types.UUIDV4,
            primaryKey: true,
        },
        title: DataTypes.STRING,
        slug: DataTypes.STRING,
        desc: DataTypes.TEXT,
        price: DataTypes.FLOAT,
        event_start: DataTypes.DATE,
        event_end: DataTypes.DATE,
        image: DataTypes.STRING,
        location: DataTypes.STRING, // only if it's not virtual
        is_virtual: DataTypes.BOOLEAN,
        virtual_link: DataTypes.STRING, // Only if the event is virtual
        is_attendees_limit: DataTypes.BOOLEAN, // attendees limit
        max_attendees: DataTypes.INTEGER,
        is_allow_guests: DataTypes.BOOLEAN,
        max_guests: DataTypes.INTEGER,
        rsvp_start: DataTypes.DATE,
        rsvp_end: DataTypes.DATE,
        is_published: DataTypes.BOOLEAN,
        is_approved: DataTypes.BOOLEAN,
        going_count: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
        waiting_count: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
        not_going_count: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
        userId: {
            type: DataTypes.UUID,
            allowNull: false,
            onDelete: "CASCADE",
            references: {
                model: "users",
                key: "id",
                as: "userId",
            },
        },
        // add tags
    },
    {
        sequelize,
        modelName: "Event",
        tableName: "events",
        createdAt: "created_at",
        updatedAt: "updated_at",
    });
    return Event;
}

export default initEvent(connection, DataTypes);