import { Model, DataTypes } from "sequelize";
import connection from "../connection";

const initPost = (sequelize, Types) => {
	class Post extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	Post.init(
		{
			id: {
				type: Types.UUID,
				defaultValue: Types.UUIDV4,
				primaryKey: true,
			},
			title: DataTypes.STRING,
			slug: DataTypes.STRING,
			short_desc: DataTypes.TEXT,
            desc: DataTypes.TEXT,
			image: DataTypes.STRING,
			userId: {
				type: DataTypes.UUID,
				allowNull: false,
				onDelete: "CASCADE",
				references: {
					model: "users",
					key: "id",
					as: "userId",
				},
			},
			catId: {
				type: DataTypes.UUID,
				onDelete: "CASCADE",
				references: {
					model: "categories",
					key: "id",
					as: "catId",
				},
			},
			promptId: {
				type: DataTypes.UUID,
				onDelete: "CASCADE",
				references: {
					model: "prompts",
					key: "id",
					as: "promptId",
				},
			},
			views: {
				type: DataTypes.INTEGER,
				defaultValue: 0,
			},
			favourites_count: {
				type: DataTypes.INTEGER,
				defaultValue: 0,
			},
			post_comments_count: {
				type: DataTypes.INTEGER,
				defaultValue: 0,
			}
		},
		{
			sequelize,
			modelName: "Post",
			tableName: "posts",
			createdAt: "created_at",
			updatedAt: "updated_at",
		}
	);
	return Post;
};
export default initPost(connection, DataTypes);
