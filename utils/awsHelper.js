import S3 from "aws-sdk/clients/s3";

const s3 = new S3({
    region: process.env.AWS_S3_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    signatureVersion: "v4",
  });

export async function deleteFromS3(url) {
    const parts = url.split('/');
    const key = parts[parts.length - 1];
    console.log(key);

    const deleteParams = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: key
    };

    return new Promise((resolve, reject) => {
        s3.deleteObject(deleteParams, (err, data) => {
            if(err) {
                console.error("Error deleting object from S3", err);
                reject(err);
            } else {
                console.log("Object deleted from S3", key);
                resolve();
            }
        });
    });
}

export async function uploadToS3(file, name) {
    return new Promise((resolve, reject) => {
            const uploadParams = {
                Bucket: process.env.AWS_S3_BUCKET_NAME,
                Key: name,
                Body: file
            };

            s3.upload(uploadParams, (err, data) => {
                if(err) {
                    console.error('Presigning post data encountered an error', err);
                    reject(err);
                } else {
                    resolve(data.Location);
                }
            });
        });
    }