This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server: test

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Bootstrap
* React Bootstrap https://react-bootstrap.netlify.app/
* Bootstrap Icons

## Forms
* Use FormData - multipart/form-data for file uploads
* Upload images to AWS S3 buckets
* Resize an image with Sharp https://sharp.pixelplumbing.com/api-constructor

## Upload files to AWS S3
#### AWS createPresignedPost() vs getSignedUrl()?
If you want to restrict users from uploading files beyond certain size, you should be using createPresignedPost, and specify ContentLength

with getSignedUrl, there is no restricting object size and user can potentially upload a 5TB object (current object limit) to s3

https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/s3-example-creating-buckets.html
https://upmostly.com/next-js/how-to-upload-a-file-to-s3-with-next-js


## Amazon SES
* https://docs.bitnami.com/aws/how-to/use-ses/

## Icons
* Flaticon
* Boxicon <i className="bx bx-comment"></i> https://boxicons.com/cheatsheet

## References 
* Stripe https://www.youtube.com/watch?v=nVvDr6MyEXE&t=730s
