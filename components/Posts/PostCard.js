import React, { useEffect, useState } from "react";
import Link from "next/link";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import PostCommentsList from "../Post/PostComments/PostCommentsList";
import PostsDetailsContent from "../SinglePosts/PostsDetailsContent";

const PostCard = ({
	// id,
	// title,
	// slug,
	// short_desc,
	// image,
	// user,
	// onFav,
	// onUnFav,
	// userId,
	// views,
	// favourites_count
	post,
	user
}) => {
	// const [fav, setfavs] = useState(false);

	// useEffect(() => {
	// 	if (userId) {
	// 		const payload = {
	// 			params: {
	// 				userId: userId,
	// 				postId: id,
	// 			},
	// 		};

	// 		const url = `${baseUrl}/api/posts/favourites/new`;
	// 		axios.get(url, payload).then((result) => {
	// 			setfavs(result.data);
	// 		});
	// 	}
	// }, []);
	return (
		<div className="col-sm-8 col-md-8 col-lg-6 mb-5 pt-3 pb-3 mx-auto shadow">
			{post && <PostsDetailsContent user={user} post={post} />}
			{/* <div className="single-courses-box">
				<div className="courses-image">
					{image && <Link href={`/post/${slug}`}>
						<a className="d-block image">
							<img src={image} alt={title} />
						</a>
					</Link>}
				</div>
				<div className="courses-content">
				{fav ? (
						<button
							className="btn"
							onClick={() => {
								onUnFav(id);
								setfavs(!fav);
							}}
						>
							<i className="bx bxs-heart"></i> {favourites_count > 0 ? favourites_count : '0' } favs
						</button>
					) : (
						<button
							className="btn"
							onClick={() => {
								onFav(id);
								setfavs(!fav);
							}}
						>
							<i className="bx bx-heart"></i> {favourites_count > 0 ? favourites_count : 0 } favs
						</button>
					)}

					{views > 0 && (<button
							className="btn view"
						>
						<i className="bx bx-book-reader"></i> {views} views
					</button>)}
					<h3>
						<Link href={`/post/${slug}`}>
							<a title={title}>{title.slice(0, 40)}...</a>
						</Link>
					</h3>

					<p>{short_desc.slice(0, 108)}</p>
					<div className="course-author d-flex align-items-center">
						<img
							src={
								user.profile_photo
									? user.profile_photo
									: "/images/avatar.png"
							}
							className="rounded-circle"
							alt="image"
						/>
						<span>{`${user.first_name} ${user.last_name}`}</span>
					</div>
					<ul className="courses-box-footer d-flex justify-content-between align-items-center">
						<li>
						<div className="course-author d-flex align-items-center">
							<img
								src={
									user.profile_photo
										? user.profile_photo
										: "/images/avatar.png"
								}
								className="rounded-circle"
								alt="image"
							/>
							<span>{`${user.first_name} ${user.last_name}`}</span>
							</div>
						</li>
						<li>
							{fav ? (
								<button
									className="btn"
									onClick={() => {
										onUnFav(id);
										setfavs(!fav);
									}}
								>
									<i className="bx bxs-heart"></i>
								</button>
							) : (
								<button
									className="btn"
									onClick={() => {
										onFav(id);
										setfavs(!fav);
									}}
								>
									<i className="bx bx-heart"></i>
								</button>
							)}
						</li>
						<li>
							<i className="flaticon-people"></i>{" "}
							{enrolments.length} Students
						</li>
					</ul>
				</div>
			</div> */}
		</div>
	);
};

export default PostCard;
