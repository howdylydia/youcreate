import React, { useState, useEffect } from "react";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";
import PostCard from "./PostCard";
import { useRouter } from "next/router";
import Pagination from "@etchteam/next-pagination";
import ShortingDropdown from "./ShortingDropdown";
import CourseSkeletonLoader from "@/utils/CourseSkeletonLoader";

const PromptPostsList = ({ user, prompt }) => {
	const [posts, setPosts] = useState([]);
	const [loading, setLoading] = useState(true);
	const [pages, setPages] = useState(0);
	const [postsCount, setPostsCount] = useState(0);
	const router = useRouter();
	const page = router.query.page ? router.query.page : "1";
	const size = router.query.size ? router.query.size : "9";
	const short = router.query.short ? router.query.short : "";
	const search = router.query.search ? router.query.search : "";
    const promptId = prompt.id;

	const fetchPosts = async () => {
		try {
            if (promptId) {
                setLoading(true);
                const payload = {
                    params: {
                        page,
                        limit: size,
                        promptId: prompt.id
                    },
                };
                const response = await axios.get(`${baseUrl}/api/all-prompt-posts`, payload);
                setPosts(response.data.posts);
                setPages(response.data.totalPages);
                setPostsCount(response.data.postsCount);
            setLoading(false);
            }
        } catch(err) {
            console.log(err.response);
        }
	};
	useEffect(() => {
		fetchPosts();
	}, [page, size, promptId]);

	const handleFav = async (postId, fav) => {
		if (!user) {
			toast.error("Need to login first.", {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
			return;
		}
		try {
			const payload = {
				userId: user.id,
				postId: postId,
				fav: fav,
			};
			const url = `${baseUrl}/api/posts/favourites/new`;
			const response = await axios.post(url, payload);

			toast.success(response.data.message, {
				style: {
					border: "1px solid #42ba96",
					padding: "16px",
					color: "#42ba96",
				},
				iconTheme: {
					primary: "#42ba96",
					secondary: "#ffffff",
				},
			});
		} catch (err) {
			console.log(err.response);
		}
	};

	return (
		<>
			<div className="courses-area courses-section pt-50 pb-100">
				<div className="container">
					<div className="col-lg-8 col-md-12 mx-auto">
						<h3>Submitted Artwork</h3>
						<div className="row">
							{loading ? (
								<CourseSkeletonLoader />
							) : (
								<>
									{posts &&
										posts.map((post) => (
											<PostCard
												key={post.id}
												post={post}
												// {...post}
												onFav={() =>
													handleFav(post.id, true)
												}
												onUnFav={() =>
													handleFav(post.id, false)
												}
												user={user}
												// userId={user && user.id}
											/>
										))}
									{postsCount > 9 && (
										<div className="col-lg-12 col-md-12">
											<div className="pagination-area text-center">
												<Pagination
													sizes={[1]}
													total={pages}
												/>
											</div>
										</div>
									)}
								</>
							)}
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default PromptPostsList;
