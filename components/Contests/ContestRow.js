import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

const ContestRow = ({
    id,
	title,
	slug,
	short_desc,
    contest_end,
    reward
}) => {
	const router = useRouter();

    const currentDate = new Date();
    //convert contest_end string to a Date object
    const endDate = new Date(contest_end);
    console.log("current date: ", currentDate, " end date: ", endDate, " currentDate > endDate: ", currentDate > endDate);
	return (
		<tr>
			<td><Link href={`/contests/${slug}`}>
							<a>{title}</a>
						</Link></td>
			<td>{short_desc}</td>
			<td>
                {contest_end}
			</td>
            <td>{reward}</td>
            <td><Link href={`/contests/${slug}`}>
							<a>{currentDate < endDate ? "Open" : "Closed"}</a>
						</Link></td>
		</tr>
	);
};

export default ContestRow;
