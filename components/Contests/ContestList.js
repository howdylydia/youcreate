import React, { useState, useEffect } from "react";
import Link from "@/utils/ActiveLink";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";
import ContestRow from "./ContestRow";
import { useRouter } from "next/router";
import Pagination from "@etchteam/next-pagination";
import CourseSkeletonLoader from "@/utils/CourseSkeletonLoader";
import GeneralLoader from "@/utils/GeneralLoader";

const ContestsList = ({ user }) => {
	const [contests, setContests] = useState([]);
	const [loading, setLoading] = useState(true);
	const [pages, setPages] = useState(0);
	const [contestsCount, setContestsCount] = useState(0);
	const router = useRouter();
	const page = router.query.page ? router.query.page : "1";
	const size = router.query.size ? router.query.size : "9";
	const short = router.query.short ? router.query.short : "";
	const search = router.query.search ? router.query.search : "";

	const fetchContests = async () => {
		setLoading(true);

		const payload = {
			params: {
				page,
				limit: size,
				short: short,
				search: search,
			},
		};
		const response = await axios.get(`${baseUrl}/api/all-contests`, payload);
		setContests(response.data.contests);
		setPages(response.data.totalPages);
		setContestsCount(response.data.contestsCount);
		setLoading(false);
	};
	useEffect(() => {
		fetchContests();
	}, [page, size, short, search]);

	return (
		<>
			<div className="courses-area courses-section pt-50 pb-100">
				<div className="container">
					<div className="section-title">
					</div>
					<div className="main-content-box">
						{loading ? (
							<GeneralLoader />
						) : (
							<div className="table-responsive">
								<table className="table table-hover align-middle fs-14">
									<thead>
										<tr>
											<th scope="col">Title</th>
											<th scope="col">
												Description
											</th>
                                            <th scope="col">Ends</th>
											<th scope="col">Reward</th>
                                            <th scope="col">Status</th>
											{/* <th scope="col">Actions</th> */}
										</tr>
									</thead>
									<tbody>
										{contests.length > 0 ? (
											contests.map((contest) => (
												
												<ContestRow
													{...contest}
													key={contest.id}
												/>
											))
										) : (
											<tr>
												<td
													colSpan="6"
													className="text-center py-3"
												>
													Empty!
												</td>
											</tr>
										)}
									</tbody>
								</table>
							</div>
						)}
                        </div>
				</div>
			</div>
		</>
	);
};

export default ContestsList;
