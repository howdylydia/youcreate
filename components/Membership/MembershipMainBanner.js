import React, { useState } from "react";
import Link from "next/link";
import { motion } from "framer-motion";
import BannerCourses from "@/components/eLearningSchool/BannerCourses";

const MembershipMainBanner = ({ user, membership }) => {
	return (
		<>
			<div className="hero">
				<div className="container-fluid">
					<div className="row align-items-center">
						<div className="col-lg-12 col-md-12">
							<div className="hero-content">
								<h1>
								Embracing the Creative Journey: A Community for All Things Art, Illustration, and Drawing
								</h1>
								<p>Express, Connect, Learn & Inspire
								</p>
                                {/* <h4><s>$5/m</s> $8/yr for the first year during the limited time *</h4>
                            	 */}
                                <p>Show your support for our growth and upkeep by becoming a valued member.</p>
								<div className="about-content">
                                    <ul className="features-list">
                                    <li>
                                        <span>
                                        <i className="flaticon-experience"></i> Weekly Drawing Prompts
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                        <i className="flaticon-time-left"></i> Share your Artwork
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                        <i className="flaticon-tutorials"></i> Virtual Events
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                        <i className="flaticon-self-growth"></i> Downloads
                                        </span>
                                    </li>
                                    </ul>
                                </div>
								<motion.div whileTap={{ scale: 0.9 }}>
									{user && !membership ? (<Link href="/membership/contribute">
										<a className="default-btn no-icon">
											Contribute Today <span></span>
										</a>
									</Link>) : (
										<Link href="/membership/contribute">
										<a className="default-btn no-icon">
											Become a Member <span></span>
										</a>
										</Link>
									)}
									{user && membership && (<Link href="/prompts">
										<a className="default-btn no-icon">
											Create Art <span></span>
										</a>
									</Link>) }
								</motion.div>
								{/* <motion.div whileTap={{ scale: 0.9 }}>
									{user ? (
										<Link href="/courses">
											<a className="default-btn">
												<i className="flaticon-user"></i>{" "}
												Browse Courses <span></span>
											</a>
										</Link>
									) : (
										<Link href="/membership/pricing">
											<a className="default-btn">
												<i className="flaticon-user"></i>{" "}
												Start Now <span></span>
											</a>
										</Link>
									)}
								</motion.div> */}
							</div>
						</div>

						{/* <div className="col-lg-6 col-md-12">
							<div className="main-banner-courses-list">
								<div className="row">
									{courses &&
										courses.map((course) => (
											<BannerCourses
												key={course.id}
												{...course}
											/>
										))}
								</div>

								<div className="banner-shape1">
									<img
										src="/images/banner-shape1.png"
										alt="image"
									/>
								</div>
								<div className="banner-shape2">
									<img
										src="/images/banner-shape2.png"
										alt="image"
									/>
								</div>
								<div className="banner-shape3">
									<img
										src="/images/banner-shape3.png"
										alt="image"
									/>
								</div>
							</div>
						</div> */}
					</div>
				</div>
			</div>
		</>
	);
};

export default MembershipMainBanner;
