import React from "react";
import {
	Accordion,
	AccordionItem,
	AccordionItemHeading,
	AccordionItemPanel,
	AccordionItemButton,
} from "react-accessible-accordion";

export default function MembershipFAQ() {
	return (
		<>
			<div className="faq-area ptb-100">
				<div className="container">
                    <div className="section-title">
						<h2>
                            F.A.Q.
						</h2>
					</div>
					<div className="faq-accordion">
						<Accordion allowZeroExpanded preExpanded={["a"]}>
							{/* <AccordionItem uuid="a">
								<AccordionItemHeading>
									<AccordionItemButton>
										How can I contact a school directly?
									</AccordionItemButton>
								</AccordionItemHeading>
								<AccordionItemPanel>
									<p>
										You can contact a school by filling out
										a{" "}
										<a href="contact.html">“Contact Us”</a>{" "}
										form. This form can be found to the
										right of both the institute and
										education program profiles and also at
										the bottom of these profiles.
									</p>
								</AccordionItemPanel>
							</AccordionItem>

							<AccordionItem uuid="b">
								<AccordionItemHeading>
									<AccordionItemButton>
										Where should I study abroad?
									</AccordionItemButton>
								</AccordionItemHeading>
								<AccordionItemPanel>
									<p>
										You can contact a school by filling out
										a{" "}
										<a href="contact.html">“Contact Us”</a>{" "}
										form. This form can be found to the
										right of both the institute and
										education program profiles and also at
										the bottom of these profiles.
									</p>
								</AccordionItemPanel>
							</AccordionItem> */}

							<AccordionItem uuid="c">
								<AccordionItemHeading>
									<AccordionItemButton>
                                        Do I keep the copyright to my artwork?
									</AccordionItemButton>
								</AccordionItemHeading>
								<AccordionItemPanel>
									<p>
                                        We value artist's intellectual property. As such, artists retain copyright to their artwork.
									</p>
								</AccordionItemPanel>
							</AccordionItem>

							<AccordionItem uuid="d">
								<AccordionItemHeading>
									<AccordionItemButton>
                                        Can I cancel my subscription?
									</AccordionItemButton>
								</AccordionItemHeading>
								<AccordionItemPanel>
									<p>
                                        Yes, you can cancel your subscription at any time.
                                    </p>
								</AccordionItemPanel>
							</AccordionItem>

							<AccordionItem uuid="e">
								<AccordionItemHeading>
									<AccordionItemButton>
                                        Wait, I still have questions.
									</AccordionItemButton>
								</AccordionItemHeading>
								<AccordionItemPanel>
									<p>If you still have questions, feel free to email us.</p>
								</AccordionItemPanel>
							</AccordionItem>
						</Accordion>
					</div>
				</div>
			</div>
		</>
	);
}
