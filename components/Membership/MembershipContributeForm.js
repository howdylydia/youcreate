import React, { useEffect, useState } from "react";
import SecurePayment from "@/components/Checkout/SecurePayment";
import { useRouter } from "next/router";
import { Modal, Button } from 'react-bootstrap';
import StripePaymentContainer from "@/components/Stripe/StripePaymentContainer";

const MembershipContributeForm = ({ user }) => {
	const [membershipAmount, setMembershipAmount] = React.useState(25);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const router = useRouter();

    function handleSetAmount(val) {
        setMembershipAmount(val);
    }

    function handleDonate() {
        setShow(true);
    }

	return (
		<>
			<div className="checkout-area ptb-100">
				<div className="container">
					<div className="row justify-content-center">
						<div className="col-md-12">
							{/* <h2>Support YouCreate</h2>
                            <p>Contribute to YouCreate and receive the access to the annual membership today.</p> */}
                            <div className="contribute">
                                <div className="contribute-content row align-items-center">
                                    {/* <form className="align-items-center"> */}
                                    <div className="col-lg-12">
                                        <div className="row align-items-center">
                                            <h2>Support YouCreate</h2>
                                            <p>The access to the events, posts, prompts and other pages are limited to members only. Make a contribution now and unlock access to our annual membership.</p>
                                        </div>
                                        <div className="d-flex multi-btns gap-3 mb-3">
                                            <button className="btn btn-outline-primary flex-fill" onClick={() => handleSetAmount(10)} value="10">$10</button>
                                            <button className="btn btn-outline-primary flex-fill" onClick={() => handleSetAmount(25)}>$25</button>
                                            <button className="btn btn-outline-primary flex-fill" onClick={() => handleSetAmount(50)}>$50</button>
                                        </div>
                                            <label className="visually-hidden" htmlFor="amountInput">Other Amount:</label>
                                            <div className="input-group">
                                                <div className="input-group-text">$</div>
                                                <input type="number" step="0.01" className="form-control" id="amountInput" 
                                                        placeholder="Other" 
                                                        value={membershipAmount}
                                                        onChange={(e) => handleSetAmount(e.target.value)}
                                                        />
                                            </div>
                                            <button className="btn btn-primary" onClick={handleDonate}>Contribute ${membershipAmount}</button>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
                    <SecurePayment />
				</div>
			</div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Pay by card</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <StripePaymentContainer user={user} itemList={[{"id":"","amount":membershipAmount*100}]}/>    
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                </Modal.Footer>
            </Modal>
		</>
	);
};

export default MembershipContributeForm;
