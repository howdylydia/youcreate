import React from 'react';
import Link from 'next/link';
import { motion } from "framer-motion";

const FeaturedMembership = ({user}) => {
  return (
    <>
      <div className="about-area bg-fef8ef ptb-100">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="about-image">
                <img src="/images/about-img1.png" alt="About" />
              </div>
            </div>

            <div className="col-lg-6 col-md-12">
              <div className="about-content">
              <span className="sub-title">MEMBERSHIP</span>
                <h2>
                    Join the art community
                </h2>
                <p>This is a beta version. If you find any issues, please report it to us.</p>
								<p>
                  Please support our development and maintenance and become a member.
                </p>
                <ul className="features-list">
                  <li>
                    <span>
                      <i className="flaticon-experience"></i> Weekly Drawing Prompts
                    </span>
                  </li>
                  <li>
                    <span>
                      <i className="flaticon-time-left"></i> Unlimited Uploads 
                    </span>
                  </li>
                  <li>
                    <span>
                      <i className="flaticon-tutorials"></i> Virtual Events
                    </span>
                  </li>
                  <li>
                    <span>
                      <i className="flaticon-self-growth"></i> Downloads
                    </span>
                  </li>
                </ul>
                
                <Link href="/membership/register">
                  <motion.div whileTap={{ scale: 0.9 }}>
									{user ? (
										<Link href="/membership/checkout">
											<a className="default-btn">
												<i className="flaticon-user"></i>{" "}
												Make a Donation <span></span>
											</a>
										</Link>
									) : (
										<Link href="/membership/register">
											<a className="default-btn">
												<i className="flaticon-user"></i>{" "}
												Start Now <span></span>
											</a>
										</Link>
									)}
								</motion.div>
                </Link>

              </div>
            </div>
          </div>
        </div>

        <div className="shape1">
          <img src="/images/shape1.png" alt="image" />
        </div>
        <div className="shape4">
          <img src="/images/shape4.png" alt="image" />
        </div>
      </div>
    </>
  )
}

export default FeaturedMembership
