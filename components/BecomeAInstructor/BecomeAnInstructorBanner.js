import React from "react";
import Link from "next/link";

const ViewAllCourses = () => {
	return (
		<>
			<div className="view-all-courses-area ptb-100 bg-F6F1ED">
				<div className="container">
					<div className="row align-items-center">
						<div className="col-lg-6 col-md-12">
							<div className="view-all-courses-content">
								<span className="sub-title">
									BECOME AN INSTRUCTOR
								</span>
								<h2>Interested in becoming an art instructor?</h2>
								<p>
									Become an instructor and teach others.  
								</p>

								<Link href="/become-an-instructor/">
									<a className="default-btn">
										<i className="flaticon-agenda"></i>{" "}
										Become an instructor <span></span>
									</a>
								</Link>
							</div>
						</div>

						<div className="col-lg-6 col-md-12">
							<div className="view-all-courses-image">
								<img
									src="/images/man-with-laptop.png"
									alt="image"
								/>
							</div>
						</div>
					</div>
				</div>

				<div className="shape1">
					<img src="/images/shape1.png" alt="image" />
				</div>
				<div className="shape9">
					<img src="/images/shape8.svg" alt="image" />
				</div>
			</div>
		</>
	);
};

export default ViewAllCourses;
