import React from "react";
import Link from "next/link";

const Features = () => {
	return (
		<>
			<div className="features-area pt-100 pb-70">
				<div className="container">
					<div className="section-title">
						<span className="sub-title">
							Posts
						</span>
						<h2>
						Trending Submissions
						</h2>
						<p>
							...
						</p>
					</div>

					<div className="row justify-content-center">
						<div className="col-lg-4 col-sm-6 col-md-6">
							<div className="single-features-box">
								<div className="icon">
									<i className="flaticon-brain-process"></i>
								</div>
								<h3></h3>
								<p>
									
								</p>
							</div>
						</div>

						<div className="col-lg-4 col-sm-6 col-md-6">
							<div className="single-features-box">
								<div className="icon">
									<i className="flaticon-computer"></i>
								</div>
								<h3></h3>
								<p>
									
								</p>
							</div>
						</div>

						<div className="col-lg-4 col-sm-6 col-md-6">
							<div className="single-features-box">
								<div className="icon">
									<i className="flaticon-shield-1"></i>
								</div>
								<h3></h3>
								<p>
									
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Features;
