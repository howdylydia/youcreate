import React, { useState, useEffect } from "react";
import Link from "@/utils/ActiveLink";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";
import EventRow from "./EventRow";
import { useRouter } from "next/router";
import Pagination from "@etchteam/next-pagination";
import CourseSkeletonLoader from "@/utils/CourseSkeletonLoader";
import GeneralLoader from "@/utils/GeneralLoader";

const EventsList = ({ user }) => {
	const [events, setEvents] = useState([]);
	const [loading, setLoading] = useState(true);
	const [pages, setPages] = useState(0);
	const [promptsCount, setPromptsCount] = useState(0);
	const router = useRouter();
	const page = router.query.page ? router.query.page : "1";
	const size = router.query.size ? router.query.size : "9";
	const short = router.query.short ? router.query.short : "";
	const search = router.query.search ? router.query.search : "";

	// const fetchEvents = async () => {
	// 	setLoading(true);

	// 	const payload = {
	// 		params: {
	// 			page,
	// 			limit: size,
	// 			short: short,
	// 			search: search,
	// 		},
	// 	};
	// 	const response = await axios.get(`${baseUrl}/api/all-prompts`, payload);
	// 	setPrompts(response.data.prompts);
	// 	setPages(response.data.totalPages);
	// 	setPromptsCount(response.data.promptsCount);
	// 	console.log(response.data.prompts[0]);
	// 	setLoading(false);
	// };
	// useEffect(() => {
	// 	fetchPrompts();
	// }, [page, size, short, search]);

	return (
		<>
			<div className="courses-area courses-section pt-50 pb-100">
				<div className="container">
					<div className="main-content-box">
						{/* {loading ? (
							<GeneralLoader />
						) : ( */}
							<div className="table-responsive">
								<table className="table table-hover align-middle fs-14">
									<thead>
										<tr>
											<th scope="col">Date & Time</th>
											<th scope="col">
												Title
											</th>
                                            <th scope="col">Duration</th>
											<th scope="col">Type</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
                                            <td>THU, Dec 7 5:15 ~ 6:45PM EDT</td>
                                            <td>Themed Drawing Night</td>
                                            <td>1.5 hrs</td>
                                            <td>Virtual</td>
                                            <td>
                                                <a
                                                    type="button"
                                                    className="btn btn-success btn-sm fs-12 ms-2"
													href="https://meet.google.com/hrv-bmdg-rct"
                                                >
                                                    Link
                                                </a>
                                            </td>
                                        </tr>
										{/* {events.length > 0 ? (
											events.map((event) => (
												
												<EventRow
													{...event}
													key={event.id}
												/>
											))
										) : (
											<tr>
												<td
													colSpan="6"
													className="text-center py-3"
												>
													Empty!
												</td>
											</tr>
										)} */}
									</tbody>
								</table>
							</div>
						{/* )} */}
					</div>
				</div>
			</div>
		</>
	);
};

export default EventsList;
