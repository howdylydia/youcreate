import React from "react";
import { useRouter } from "next/router";

const PromptRow = ({
    id,
	title,
	slug,
	short_desc,
	image,
    category
}) => {
	const router = useRouter();
	return (
		<tr>
			<td>
				{image && <img
					src={image}
					alt="image"
					className="w-55px"
				/>}
			</td>
			<td>{title}</td>
			<td>{short_desc}</td>
			<td>
				<div className="max-300px max-height-60">{category && category.name}</div>
			</td>
			<td>
				<button
					type="button"
					className="btn btn-success btn-sm fs-12 ms-2"
					onClick={() => router.push(`/prompt/${slug}`)}
				>
					View
				</button>
			</td>
		</tr>
	);
};

export default PromptRow;
