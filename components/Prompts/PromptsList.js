import React, { useState, useEffect } from "react";
import Link from "@/utils/ActiveLink";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";
import PromptCard from "./PromptCard";
import PromptRow from "./PromptRow";
import { useRouter } from "next/router";
import Pagination from "@etchteam/next-pagination";
import SortingDropdown from "./SortingDropdown";
import CourseSkeletonLoader from "@/utils/CourseSkeletonLoader";
import GeneralLoader from "@/utils/GeneralLoader";

const PromptsList = ({ user }) => {
	const [prompts, setPrompts] = useState([]);
	const [loading, setLoading] = useState(true);
	const [pages, setPages] = useState(0);
	const [promptsCount, setPromptsCount] = useState(0);
	const router = useRouter();
	const page = router.query.page ? router.query.page : "1";
	const size = router.query.size ? router.query.size : "9";
	const short = router.query.short ? router.query.short : "";
	const search = router.query.search ? router.query.search : "";

	const fetchPrompts = async () => {
		setLoading(true);

		const payload = {
			params: {
				page,
				limit: size,
				short: short,
				search: search,
			},
		};
		const response = await axios.get(`${baseUrl}/api/all-prompts`, payload);
		setPrompts(response.data.prompts);
		setPages(response.data.totalPages);
		setPromptsCount(response.data.promptsCount);
		setLoading(false);
	};
	useEffect(() => {
		fetchPrompts();
	}, [page, size, short, search]);

	return (
		<>
			<div className="courses-area courses-section pt-50 pb-100">
				<div className="container">
					{/* <SortingDropdown /> */}
					<div className="section-title">
						{/* <span className="sub-title">A new prompt will be released every week starting from October!</span> */}
					</div>
					<div className="main-content-box">
						{loading ? (
							<GeneralLoader />
						) : (
							<div className="table-responsive">
								<table className="table table-hover align-middle fs-14">
									<thead>
										<tr>
											<th scope="col">Image</th>
											<th scope="col">Title</th>
											<th scope="col">
												Description
											</th>
											<th scope="col">Category</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>
										{prompts.length > 0 ? (
											prompts.map((prompt) => (
												
												<PromptRow
													{...prompt}
													key={prompt.id}
												/>
											))
										) : (
											<tr>
												<td
													colSpan="6"
													className="text-center py-3"
												>
													Empty!
												</td>
											</tr>
										)}
									</tbody>
								</table>
							</div>
						)}
					</div>
				</div>
			</div>
		</>
	);
};

export default PromptsList;
