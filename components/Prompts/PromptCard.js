import React, { useEffect, useState } from "react";
import Link from "next/link";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";

const PromptCard = ({
	id,
	title,
	slug,
	short_desc,
	image
}) => {

	return (
		<div className="col-lg-4 col-md-6">
			<div className="single-courses-box">
				<div>
					<h3>
						<Link href={`/prompt/${slug}`}>
							<a title={title}>{title.slice(0, 40)}...</a>
						</Link>
					</h3>
				</div>
				<div className="courses-image">
					<Link href={`/prompt/${slug}`}>
						<a className="d-block image">
							<img src={image} alt={title} />
						</a>
					</Link>

				</div>
				<div className="courses-content">
					{/* <div className="course-author d-flex align-items-center">
						<img
							src={
								user.profile_photo
									? user.profile_photo
									: "/images/avatar.png"
							}
							className="rounded-circle"
							alt="image"
						/>
						<span>{`${user.first_name} ${user.last_name}`}</span>
					</div> */}
					<ul className="courses-box-footer d-flex justify-content-between align-items-center">
					</ul>
				</div>
			</div>
		</div>
	);
};

export default PromptCard;
