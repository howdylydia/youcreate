import React from "react";
import { motion } from "framer-motion";
import baseUrl from "@/utils/baseUrl";
import axios from "axios";
import toast from "react-hot-toast";

/* 
1. A user submits the reset password request by entering their email address.
2. A user receives an email with token leading to the reset password form (new password - confirm password)
-- verifies the token
3. The user submits the form to reset the password.

*/
const PasswordResetForm = ({token}) => {
	console.log(token);
	const handleSubmit = async (e) => {
		e.preventDefault();
		console.log("e", e);
		try {
			const passwordVal = e.target.elements.password.value;
			console.log("passwordVal", passwordVal);
			const payload = {token, password: passwordVal};
			const url = `${baseUrl}/api/account/password-reset`;
			const response = await axios.post(url, payload);
			console.log("res", response);
			toast.success(response.data.message, {
				style: {
					border: "1px solid #4BB543",
					padding: "16px",
					color: "#4BB543",
				},
				iconTheme: {
					primary: "#4BB543",
					secondary: "#FFFAEE",
				},
			});
		} catch (err) {
			let message = "An error occurred, but the response structure is unexpected.";
			// Check if err.response and err.response.data exist before destructuring
			if (err.response && err.response.data && err.response.data.message) {
				message = err.response.data.message || err.response.data.error.message;
			}
			toast.error(message, {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
		} finally {
		}
	};

	return (
		<>
			<div className="ptb-100">
				<div className="container">
					<div className="row justify-content-center">
						<div className="col-lg-6">
							<div className="login-form">
								<p>Please reset your password.</p>

								<form onSubmit={handleSubmit}>
									<div className="form-group">
										<label>Password</label>
										<input
											type="text"
											className="form-control"
											placeholder="password"
											name="password"
										/>
									</div>

									<motion.button
										type="submit"
										whileTap={{ scale: 0.9 }}
									>
										Submit
									</motion.button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default PasswordResetForm;
