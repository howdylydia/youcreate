import React, {useState, useEffect} from "react";
import { motion } from "framer-motion";
import Router from "next/router";
import LoadingSpinner from "@/utils/LoadingSpinner";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";
import axios from "axios";

const INITIAL_USER = {
	email: ""
}

const ForgotPasswordForm = () => {
	const [disabled, setDisabled] = useState(true);
	const [loading, setLoading] = useState(false);
	
	const handleSubmit = async (e) => {
		e.preventDefault();
		const emailVal = e.target.elements.email.value;

		try {
			setLoading(true);
			const url = `${baseUrl}/api/account/forgot-password`;
			const payload = {email: emailVal};
			const response = await axios.post(url, payload);
			toast.success(response.data.message, {
				style: {
					border: "1px solid #4BB543",
					padding: "16px",
					color: "#4BB543",
				},
				iconTheme: {
					primary: "#4BB543",
					secondary: "#FFFAEE",
				},
			});
		} catch (err) {
			let message = "An error occurred, but the response structure is unexpected.";
			// Check if err.response and err.response.data exist before destructuring
			if (err.response && err.response.data && err.response.data.message) {
				message = err.response.data.message || err.response.data.error.message;
			}
			toast.error(message, {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
		} finally {
			setLoading(false);
		}
	}
	return (
		<>
			<div className="ptb-100">
				<div className="container">
					<div className="row justify-content-center">
						<div className="col-lg-6">
							<div className="login-form">
								<p>Please enter Email address.</p>
								<form onSubmit={handleSubmit}>
									<div className="form-group">
										<label>Email</label>
										<input
											type="text"
											className="form-control"
											placeholder="Email"
											name="email"
										/>
									</div>

									<motion.button
										type="submit"
										whileTap={{ scale: 0.9 }}
									>
										Send
									</motion.button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default ForgotPasswordForm;
