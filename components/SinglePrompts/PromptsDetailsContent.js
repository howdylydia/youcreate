import React, { useEffect, useState } from "react";
import Link from "next/link";
import { formatDate } from "@/utils/helper";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import {Modal, Button} from 'react-bootstrap';
import PostCreateForm from "../Post/PostCreateForm";

const PromptsDetailsContent = ({ user: current_user, prompt }) => {
	const router = useRouter();
	const {
		id,
		title,
		slug,
		short_desc,
		image,
		updated_at,
		category
	} = prompt;

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	return (
		<>
			<div className="courses-details-area ptb-50">
				<div className="container">
					<div className="courses-details-header">
						<div className="row align-items-center">
							<div className="col-lg-8 col-md-12 mx-auto">
								<div className="courses-meta">
									<ul>
										<li>
											<i className="bx bx-folder-open"></i>
											<span>Category</span>
											{category && (
												<Link
													href={`/category/${category.slug}`}
												>
													<a>{category.name}</a>
												</Link>
											)}
										</li>
										<li>
											<i className="bx bx-calendar"></i>
											<span>Last Updated</span>
											<Link href="#">
												<a>{formatDate(updated_at)}</a>
											</Link>
										</li>
										<li className="float-end">
											<button className="btn btn-primary"
												onClick={handleShow}>Submit Your Art</button>
										</li>
									</ul>
								</div>
							</div>

						</div>
					</div>

					<div className="row">
						<div className="col-lg-8 col-md-12 mx-auto">
							<div className="courses-details-image-style-two">
								{image && <img src={image} alt="image" />}
								<p>{short_desc}</p>
							</div>

							{/* <div className="courses-details-desc-style-two">
								

								
									<div className="mb-4">
									
										
									</div>
							

			
							</div> */}
						</div>

						{/* <div className="col-lg-4 col-md-12">
						<ul>
										<li>
											<i className="bx bx-folder-open"></i>
											<span>Category</span>
											{category && (
												<Link
													href={`/category/${category.slug}`}
												>
													<a>{category.name}</a>
												</Link>
											)}
										</li>
										<li>
											<i className="bx bx-calendar"></i>
											<span>Last Updated</span>
											<Link href="#">
												<a>{formatDate(updated_at)}</a>
											</Link>
										</li>
									</ul>
						</div> */}
					</div>
				</div>
			</div>
			<Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
				<	Modal.Title>Submit Your Art for {prompt.title}</Modal.Title>
				</Modal.Header>
				<Modal.Body><PostCreateForm promptId={prompt.id}/></Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleClose}>
						Close
					</Button>
					{/* <Button variant="primary" onClick={handleClose}>
						Save Changes
					</Button> */}
				</Modal.Footer>
			</Modal>
		</>
	);
};

export default PromptsDetailsContent;
