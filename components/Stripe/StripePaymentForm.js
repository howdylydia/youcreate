import React from "react";
import {
  PaymentElement,
  LinkAuthenticationElement,
  useStripe,
  useElements
} from "@stripe/react-stripe-js";
import LoadingSpinner from "@/utils/LoadingSpinner";
import { useEffect, useState } from "react";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";

export default function StripePaymentForm({totalAmount, user, clientSecret}) {
  const stripe = useStripe();
  const elements = useElements();
  const [email, setEmail] = useState(user && user.email || '');
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!stripe) {
      return;
    }
    
    if (!clientSecret) {
      return;
    }

    // stripe.retrievePaymentIntent(clientSecret).then(({ paymentIntent }) => {
    //   console.log("payment status ", paymentIntent.status);
    //   switch (paymentIntent.status) {
    //     case "succeeded": {
    //       toast.success("Payment succeeded!", {
    //         style: {
    //           border: "1px solid #4BB543",
    //           padding: "16px",
    //           color: "#4BB543",
    //         },
    //         iconTheme: {
    //           primary: "#4BB543",
    //           secondary: "#FFFAEE",
    //         },
    //       });
    //       setMessage("Payment succeeded!");
    //       break; 
    //     }
    //     case "processing": {
    //       toast("Your payment is processing.", {
    //         icon: '🕥',
    //       });
    //       setMessage("Your payment is processing.");
    //       break;
    //     }
    //     case "requires_payment_method": {
    //       toast.error("Your payment was not successful, please try again.");
    //       setMessage("Your payment was not successful, please try again.");
    //       break;
    //     }
    //     default: {
    //       toast.error("Something went wrong.");
    //       setMessage("Something went wrong.");
    //       break;
    //     }
    //   }
    // });
  }, [stripe]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    setIsLoading(true);
    try {
      const { error } = await stripe.confirmPayment({
        elements,
        confirmParams: {
          return_url: `${baseUrl}/checkout-completion`,
          receipt_email: email,
        },
      });

      if (error) {
        // Handle card error
        setError(error.message);
        toast.error(error.message, {
          style: {
            border: '1px solid #ff0033',
            padding: '16px',
            color: '#ff0033',
          },
          iconTheme: {
            primary: '#ff0033',
            secondary: '#FFFAEE',
          },
        });
      }
    } catch (err) {
      // Handle other errors
      setError('An error occurred while processing your payment.');
      console.error(err);
    } finally {
      setIsLoading(false);
    }
  };


  const paymentElementOptions = {
    layout: "tabs",
  };

  return (
    <div className="contribute">
    <form id="payment-form" onSubmit={handleSubmit}>
      <LinkAuthenticationElement
        id="link-authentication-element"
        onChange={(e) => e.target && setEmail(e.target.value)}
      />
      <PaymentElement id="payment-element" options={paymentElementOptions} />
      <button disabled={isLoading || !stripe || !elements} id="submit" className="btn btn-primary mt-2">
        <span id="button-text">
          {isLoading ? (<>Processing... <LoadingSpinner /></>) : (`Pay $${totalAmount/100}`)}
        </span>
      </button>
      {/* Show any error or success messages */}
      {error && <p style={{ color: 'red' }}>{error}</p>}
    </form>
    </div>
  );
}