import React from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import StripePaymentForm from "./StripePaymentForm";
import { parseCookies } from "nookies";
import { useEffect, useState } from "react";

// Make sure to call loadStripe outside of a component’s render to avoid
// recreating the Stripe object on every render.
// This is your test publishable API key.
const stripePromise = loadStripe(process.env.STRIPE_PUBLISHABLE_KEY);

export default function StripePaymentContainer({itemList, user}) {
  const [clientSecret, setClientSecret] = useState("");
  const { elarniv_users_token } = parseCookies();

  // itemList is in this format: [{ id: "xl-tshirt", amount: 1000 }]
  useEffect(() => {
    // Create PaymentIntent as soon as the page loads
    fetch("/api/stripe/create-payment-intent", {
      method: "POST",
      headers: { "Content-Type": "application/json", Authorization: elarniv_users_token, },
      body: JSON.stringify({ items: itemList }),
    })
    .then((res) => {
      if (!res.ok) {
        throw new Error("Network response was not ok");
      }
      return res.json();
    })
    .then((data) => {
      setClientSecret(data.clientSecret);
    })    
    .catch((error) => {
      console.error("Error fetching PaymentIntent:", error);
      // Handle the error (e.g., show an error message to the user)
    });
  }, []);

  const appearance = {
    theme: 'stripe',
  };
  const options = {
    clientSecret,
    appearance,
  };

  const fCalculateTotal = (list) => {
    let totalVal = 0;
    list.map(item => {
      totalVal += item.amount;
    })
    return totalVal;
  }

  return (
    <div className="stripe-payment-container">
      {clientSecret && (
        <Elements options={options} stripe={stripePromise}>
          <StripePaymentForm user={user} clientSecret={clientSecret} totalAmount={fCalculateTotal(itemList)}/>
        </Elements>
      )}
    </div>
  );
}