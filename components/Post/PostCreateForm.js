import React, { useState, useEffect } from "react";
import controls from "@/utils/RTEControl";
import dynamic from "next/dynamic";

const RichTextEditor = dynamic(() => import("@mantine/rte"), {
	ssr: false,
	loading: () => null,
});
import axios from "axios";
import { parseCookies } from "nookies";
import baseUrl from "@/utils/baseUrl";
import LoadingSpinner from "@/utils/LoadingSpinner";
import toast from "react-hot-toast";
import { useRouter } from "next/router";

const INITIAL_VALUE = {
	title: "",
	short_desc: "",
	catId: ""
};

const PostCreateForm = ({ btnText, is_class, promptId }) => {
	const { elarniv_users_token } = parseCookies();
	const [post, setPost] = useState(INITIAL_VALUE);
	const [disabled, setDisabled] = React.useState(true);
	const [loading, setLoading] = React.useState(false);
	const [categories, setCategories] = useState([]);
	const [imagePreview, setImagePreview] = React.useState("");
	const router = useRouter();

	useEffect(() => {
		//const isPost = Object.values(post).map((el) => {console.log("el", el); console.log("Boolean", Boolean(el)); Boolean(el);});
		post.title != "" ? setDisabled(false) : setDisabled(true);
	}, [post]);

	useEffect(() => {
		const fetchData = async () => {
			const payload = {
				headers: { Authorization: elarniv_users_token },
			};
			const response = await axios.get(
				`${baseUrl}/api/categories`,
				payload
			);
			setCategories(response.data.categories);
		};

		fetchData();
	}, []);

	const handleChange = (e) => {
		const { name, value, files } = e.target;

		// set preview
		if (name === "image") {
			setPost((prevState) => ({
				...prevState,
				image: files[0],
			}));
			setImagePreview(window.URL.createObjectURL(files[0]));
		} else {
			setPost((prevState) => ({ ...prevState, [name]: value }));
		}
	};

	const handleSubmit = async (e) => {
		e.preventDefault();

		const payloadData = new FormData();
		payloadData.append("title", post.title);
		payloadData.append("short_desc", post.short_desc);
		payloadData.append("catId", post.catId);
		payloadData.append("image", post.image);
		payloadData.append("promptId", promptId || null);
		
		try {
			setLoading(true);
			// Check: Display the key/value pairs
			// for (var pair of payloadData.entries()) {
			// 	console.log(pair[0]+ ', ' + pair[1]); 
			// }
				
			const payloadHeader = {
				headers: { 
					Authorization: elarniv_users_token,
					"Content-Type": "multipart/form-data",
				 },
			};

			const url = `${baseUrl}/api/posts/new`;
			const response = await axios.post(url, payloadData, payloadHeader);
			setLoading(false);
			
			toast.success(response.data.message, {
				style: {
					border: "1px solid #4BB543",
					padding: "16px",
					color: "#4BB543",
				},
				iconTheme: {
					primary: "#4BB543",
					secondary: "#FFFAEE",
				},
			});
			
			router.push(`/post/${response.data.post.slug}`);
		} catch (err) {
			console.log(err);
			let message = "An error occurred, but the response structure is unexpected.";
			// Check if err.response and err.response.data exist before destructuring
			if (err.response && err.response.data && err.response.data.message) {
				message = err.response.data.message || err.response.data.error.message;
			}
			toast.error(message, {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
		} finally {
			setLoading(false);
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<div className="row">
				<div className="col-md-6">
					<div className="form-group">
						<label className="form-label fw-semibold">
							Title *
						</label>
						<input
							type="text"
							className="form-control"
							placeholder="Post Title"
							name="title"
							value={post.title}
							onChange={handleChange}
							required={true}
						/>
					</div>
				</div>


				<div className="col-md-6">
					<div className="form-group">
						<label className="form-label fw-semibold">
							Category
						</label>
						<select
							className="form-select"
							name="catId"
							value={post.catId}
							onChange={handleChange}
						>
							<option value="">Select</option>
							{categories.length > 0 &&
								categories.map((cat) => (
									<option key={cat.id} value={cat.id}>
										{cat.name}
									</option>
								))}
						</select>
					</div>
				</div>

				<div className="col-md-6">
					<div className="form-group">
						<label className="form-label fw-semibold">
							Image
						</label>
						<input
							type="file"
							className="form-control file-control"
							name="image"
							accept="image/*"
							onChange={handleChange}
						/>
						<div className="form-text">
							Accept image files only (i.e. JPEG, PNG, GIF, BMP, and so on). Doesn't support transparent background.
						</div>

						{imagePreview && (<div className="mt-2">
							<img
								src={
									imagePreview
								}
								alt="image"
								className="img-thumbnail w-100px me-2"
							/>
						</div>)}
					</div>
				</div>

				<div className="col-md-12">
					<div className="form-group">
						<label className="form-label fw-semibold">
							Description
						</label>
						<textarea
							className="form-control"
							name="short_desc"
							value={post.short_desc}
							onChange={handleChange}
						/>
						{/* <div className="form-text">
							The description will highlight all available areas.
						</div> */}
					</div>
				</div>

				{/* <div className="col-md-12">
					<div className="form-group">
						<label className="form-label fw-semibold">
							Description
						</label>
						<RichTextEditor
							controls={controls}
							value={post.overview}
							onChange={(e) =>
								setPost((prevState) => ({
									...prevState,
									overview: e,
								}))
							}
						/>
					</div>
				</div> */}
				{/* <div className="col-md-6">
					<div className="form-group">
						<label className="form-label fw-semibold">
							Requirements
						</label>
						<RichTextEditor
							controls={controls}
							value={post.requirements}
							onChange={(e) =>
								setPost((prevState) => ({
									...prevState,
									requirements: e,
								}))
							}
						/>
					</div>
				</div>
				<div className="col-md-6">
					<div className="form-group">
						<label className="form-label fw-semibold">
							What You Will Learn
						</label>
						<RichTextEditor
							controls={controls}
							value={post.what_you_will_learn}
							onChange={(e) =>
								setPost((prevState) => ({
									...prevState,
									what_you_will_learn: e,
								}))
							}
						/>
					</div>
				</div>
				<div className="col-md-6">
					<div className="form-group">
						<label className="form-label fw-semibold">
							Who Is This Post For?
						</label>
						<RichTextEditor
							controls={controls}
							value={post.who_is_this_post_for}
							onChange={(e) =>
								setPost((prevState) => ({
									...prevState,
									who_is_this_post_for: e,
								}))
							}
						/>
					</div>
				</div> */}

				<div className="col-12">
					<button
						type="submit"
						className="default-btn"
						disabled={disabled}
					>
						<i className="flaticon-right-arrow"></i>
						{btnText || "Submit"} <span></span>
						{loading ? <LoadingSpinner /> : ""}
					</button>
				</div>
			</div>
		</form>
	);
};

export default PostCreateForm;
