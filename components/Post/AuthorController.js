import React from 'react';
import { useRouter } from "next/router";

const AuthorController = ({id, onDelete}) => {
    const router = useRouter();

    return(<>
        <div>
                 <button
					onClick={() => onDelete(id)}
					type="button"
					className="btn btn-danger btn-sm fs-12"
				>
					Delete
				</button>
				<button
					type="button"
					className="btn btn-success btn-sm fs-12 ms-2"
					onClick={() => router.push(`/admin/${id}`)}
				>
					Edit
				</button>
        </div>
        </>);
}

export default AuthorController;