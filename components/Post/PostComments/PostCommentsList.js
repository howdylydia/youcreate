import React, { useState, useEffect } from "react";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";
import PostCommentRow from "./PostCommentRow";
import PostCommentNew from "./PostCommentNew";
import { useRouter } from "next/router";
import Pagination from "@etchteam/next-pagination";
import CourseSkeletonLoader from "@/utils/CourseSkeletonLoader";

const PostCommentsList = ({ user, postId, updateCommentsCount }) => {
	const [postComments, setPostComments] = useState([]);
	const [loading, setLoading] = useState(true);
	const [pages, setPages] = useState(0);
	const [postCommentsCount, setPostCommentsCount] = useState(0);
	const router = useRouter();
	const page = router.query.page ? router.query.page : "1";
	const size = router.query.size ? router.query.size : "9";

	const updateCommentsList = (newComment) => {
		setPostComments([...postComments, newComment]);
		setPostCommentsCount(postCommentsCount+1);
	}

	const handleCommentDelete = (deleteCommentId) => {
		// filter out deleted comment from the commet list
		const updatedComments = postComments.filter((comment) => comment.id !== deleteCommentId);
		setPostComments(updatedComments);

		// update comments count in postsdetailscontent
		updateCommentsCount(false);
	}
	
	const fetchPostComments = async () => {
		if (postId) {
			
			setLoading(true);

			const payload = {
				params: {
					page,
					limit: size,
					postId: postId
				},
			};
			const response = await axios.get(`${baseUrl}/api/all-post-comments`, payload);
			setPostComments(response.data.post_comments);
			setPages(response.data.totalPages);
			setPostCommentsCount(response.data.post_commentsCount);
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchPostComments();
	}, [page, size,]);

	return (
		<>
			<div className="comments-container pt-1 pb-1">
				{loading ? (
							<div>Comments loading...</div>
						) : (
							<>
							{/* <h4><i className="bx bx-comment"></i> Comments - {postCommentsCount}</h4> */}
				<div className="post-comment-list-container">
					<ul className="post-comment-list">
						{postComments &&
							postComments.map((postComment) => (
								<PostCommentRow
									key={postComment.id}
									{...postComment}
									current_user_id={user && user.id}
									onDeleteSuccess={handleCommentDelete}
								/>
							))}
					</ul>
					{postCommentsCount > 9 && (
						<div className="col-lg-12 col-md-12">
							<div className="pagination-area text-center">
								<Pagination
									sizes={[1]}
									total={pages}
								/>
							</div>
						</div>
					)}
					
				</div>
				<div className="panel-google-plus-comment">
					{user && (
						<PostCommentNew 
							user={user} 
							postId={postId} 
							updateCommentsList={updateCommentsList}
							updateCommentsCount={updateCommentsCount}
						/>
					)}
				</div>
				</>
				)}
			</div>
		</>
	);
};

export default PostCommentsList;
