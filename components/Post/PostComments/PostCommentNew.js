import React, { useEffect, useState } from "react";
import Link from "next/link";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from 'react-hot-toast';
import { parseCookies } from "nookies";
import LoadingSpinner from "@/utils/LoadingSpinner";

const PostCommentNew = ({
	postId,
	user,
	updateCommentsList,
	updateCommentsCount
}) => {
	const { elarniv_users_token } = parseCookies();
	const [loading, setLoading] = useState(false);
	const [comment, setComment] = useState("");

	const handleChange = (e) => {
		const { name, value } = e.target;
		if (name === "comment") {
			const commentValue = value;
			setComment(value);
		}
	};

	const handleSubmit = async (e) => {
		e.preventDefault();

		try {
			setLoading(true);
			const payloadHeader = {
				headers: { 
					Authorization: elarniv_users_token
				 },
			};
			const payloadData = {
				comment, 
				postId
			}
			const url = `${baseUrl}/api/posts/post/comment/new`;
			const response = await axios.post(url, payloadData, payloadHeader);
			setLoading(false);
			
			toast.success(response.data.message, {
				style: {
					border: "1px solid #4BB543",
					padding: "16px",
					color: "#4BB543",
				},
				iconTheme: {
					primary: "#4BB543",
					secondary: "#FFFAEE",
				},
			});
			
			//router.push(`/post/${response.data.post.slug}`);
			updateCommentsList(response.data.newComment);
			updateCommentsCount(true);
			setComment(""); // reset comment
		} catch (err) {
			let message = "An error occurred, but the response structure is unexpected.";
			// Check if err.response and err.response.data exist before destructuring
			if (err.response && err.response.data && err.response.data.message) {
				message = err.response.data.message || err.response.data.error.message;
			}
			toast.error(message, {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
		} finally {
			setLoading(false);
		}

	}

	return (
	<li>
		<div className="post-comment-form d-flex">
			<div className="avatar">
				{user && (<img
					src={
						user.profile_photo
							? user.profile_photo
							: "/images/avatar.png"
					}
					className="rounded-circle flex-shrink-0"
					alt="My Image"
				/>)
				}
			</div>
			{/* <div className="user-details"> */}
			<form onSubmit={handleSubmit} className="d-flex">
				<div className="form-group me-2">
					<label className="d-none">Comment</label>
					<textarea
						rows="2"
						type="text"
						className="form-control"
						name="comment"
						value={comment}
						onChange={handleChange}
					/>
				</div>
				<div>
					<button
						type="submit"
						className="btn btn-primary"
						// onClick={() => handleSubmit()}
						// disabled={disabled}
					>
						Post
						{loading ? <LoadingSpinner /> : ""}
					</button>
				</div>
			</form>
		</div>
	</li>
	);
};

export default PostCommentNew;
