import React, { useEffect, useState } from "react";
import Link from "next/link";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import { X } from "react-bootstrap-icons";
import { formatDate } from "@/utils/helper";
import { confirmAlert } from "react-confirm-alert";
import toast from "react-hot-toast";
import { parseCookies } from "nookies";

const PostCommentRow = ({
	id,
	comment,
	updated_at,
	user: comment_author,
	userId,
	current_user_id,
	onDeleteSuccess
}) => {
	const { elarniv_users_token } = parseCookies();

	const confirmDelete = (id) => {
		confirmAlert({
			title: "Confirm to Delete",
			message:
				"Are you sure you would like to delete this comment?",
			buttons: [
				{
					label: "Yes",
					onClick: () => handleDelete(id),
				},
				{
					label: "No",
				},
			],
		});
	}

	const handleDelete = async (id) => {
		try {
			const payload = {
				headers: { Authorization: elarniv_users_token },
				params: { id },
			};
			const response = await axios.delete(
				`${baseUrl}/api/posts/post/comment/${id}`,
				payload
			);
			
			// update parent component to filter out this comment
			onDeleteSuccess(id);
			
			toast.success(response.data.message, {
				style: {
					border: "1px solid #4BB543",
					padding: "16px",
					color: "#4BB543",
				},
				iconTheme: {
					primary: "#4BB543",
					secondary: "#FFFAEE",
				},
			});
			
		} catch (err) {
			let message = "An error occurred, but the response structure is unexpected.";

			// Check if err.response and err.response.data exist before destructuring
			if (err.response && err.response.data && err.response.data.message) {
				message = err.response.data.message || err.response.data.error.message;
			}
			toast.error(message, {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
		} finally {
			//fetchData();
		}
	}

	return (
		<li className="post-comment-list d-flex">
			<div className="post-user-container d-flex">
				<div className="avatar">
					{comment_author && (<img
						src={
							comment_author.profile_photo
								? comment_author.profile_photo
								: "/images/avatar.png"
						}
						className="rounded-circle flex-shrink-0"
						alt={`${comment_author.first_name} Profile Photo`}
					/>)
					}
				</div>
				<div className="post-comment-details">
					<div>{comment_author && `${comment_author.first_name} ${comment_author.last_name}`}</div>
					<div>{comment}</div>
					<div>{formatDate(updated_at)}</div>
				</div>
			</div>
			{current_user_id == (userId || comment_author.id) && (
				<div className="post-comment-actions">
					<button className="btn btn-danger" onClick={() => {confirmDelete(id)}}><X/></button>
				</div> 
			)
			}
		</li>
					
	);
};

export default PostCommentRow;
