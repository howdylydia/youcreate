import React from "react";

const PostOverview = ({ overview }) => {
	return (
		<div className="courses-details-desc-style-two">
			<h3>About this post</h3>
			<div dangerouslySetInnerHTML={{ __html: overview }}></div>
		</div>
	);
};

export default PostOverview;
