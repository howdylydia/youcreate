import React, { useEffect, useState } from "react";
import StickyBox from "react-sticky-box";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";
import toast from "react-hot-toast";
import baseUrl from "@/utils/baseUrl";
import axios from "axios";
import { useRouter } from "next/router";
import SocialShareBtns from "./SocialShareBtns";
import { calculateDiscount } from "@/utils/helper";

const PostsDetailsSidebar = ({ current_user, post, onCoupon }) => {
	// console.log(post);
	const cartItems = useSelector((state) => state.cart.cartItems);
	const discount = useSelector((state) => state.cart.discount);
	const dispatch = useDispatch();
	const [add, setAdd] = useState(false);
	const [alreadyBuy, setAlreadyBuy] = useState(false);
	const router = useRouter();
	const [apply, setApplyCoupon] = useState(false);
	const [coupon, setCoupon] = useState({ coupon: "" });

	return (
		<>
			<StickyBox className="sticky-box" offsetTop={20} offsetBottom={20}>
				<div className="courses-sidebar-sticky">
					<div className="courses-sidebar-information">
						<SocialShareBtns />
					</div>
				</div>
			</StickyBox>
		</>
	);
};

export default PostsDetailsSidebar;
