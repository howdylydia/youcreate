import React, { useEffect, useState } from "react";
import Link from "next/link";
import PostCommentsList from "../Post/PostComments/PostCommentsList";
import { formatDate } from "@/utils/helper";
import BuyPostBtn from "./BuyPostBtn";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { confirmAlert } from "react-confirm-alert";
import toast from "react-hot-toast";
import { parseCookies } from "nookies";
import axios from "axios";
import { Eye, Heart, HeartFill, Chat, Pencil, Trash } from "react-bootstrap-icons";
import { Button, Dropdown } from 'react-bootstrap';
import Collapse from 'react-bootstrap/Collapse';
import baseUrl from "@/utils/baseUrl";

const PostsDetailsContent = ({ user: current_user, post }) => {
	const {
		id,
		title,
		slug,
		short_desc,
		image,
		updated_at,
		category,
		user: author,
		userId: authorId,
		views,
		favourites_count,
		post_comments_count
	} = post;
	const router = useRouter();
	const [open, setOpen] = useState(true);
	const [commentsCount, setCommentsCount] = useState(post.post_comments_count || 0);
	const [favCount, setFavCount] = useState(post.favourites_count|| 0);
	const discount = useSelector((state) => state.cart.discount);
	const { elarniv_users_token } = parseCookies();
	const [isFav, setIsFav] = useState(false);

	useEffect(() => {
		if (current_user.id && post.id) {
			const payload = {
				params: {
					userId: current_user.id,
					postId: post.id,
				},
			};

			// check if the user already favourited this post
			const url = `${baseUrl}/api/posts/favourites/new`;
			axios.get(url, payload).then((result) => {
				setIsFav(result.data);
			});
		}
	}, [favCount, commentsCount]);

	const setFav = async () => {
		try {
			const payloadHeader = {
				headers: { 
					Authorization: elarniv_users_token
				 },
			};
			const payloadData = {
				userId: current_user.id,
				postId: post.id,
			};
			const url = `${baseUrl}/api/posts/favourites/new`;
			const response = await axios.post(url, payloadData, payloadHeader);
			// success
			setIsFav(response.data.status);
			setFavCount(response.data.favourites_count);
		} catch (err) {
			let message = "An error occurred, but the response structure is unexpected.";
			// Check if err.response and err.response.data exist before destructuring
			if (err.response && err.response.data && err.response.data.message) {
				message = err.response.data.message || err.response.data.error.message;
			}
			toast.error(message, {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
		}
	}

	const updateCommentsCount = (increment = true) => {
		if (increment) {
			setCommentsCount(commentsCount + 1); // Increment the count
		} else {
			setCommentsCount(commentsCount - 1); // Decrement the count
		};
	};

	const confirmDelete = (id) => {
		confirmAlert({
			title: "Confirm to Delete",
			message:
				"Are you sure you would like to delete this post?",
			buttons: [
				{
					label: "Yes",
					onClick: () => handleDelete(id),
				},
				{
					label: "No",
				},
			],
		});
	};

	const handleDelete = async (id) => {
		try {
			const payload = {
				headers: { Authorization: elarniv_users_token },
				params: { id },
			};
			const response = await axios.delete(
				`${baseUrl}/api/posts/post/${id}`,
				payload
			);
			toast.success(response.data.message, {
				style: {
					border: "1px solid #4BB543",
					padding: "16px",
					color: "#4BB543",
				},
				iconTheme: {
					primary: "#4BB543",
					secondary: "#FFFAEE",
				},
			});
			
		} catch (err) {
			let message = "An error occurred, but the response structure is unexpected.";

			// Check if err.response and err.response.data exist before destructuring
			if (err.response && err.response.data && err.response.data.message) {
				message = err.response.data.message || err.response.data.error.message;
			}
			toast.error(message, {
				style: {
					border: "1px solid #ff0033",
					padding: "16px",
					color: "#ff0033",
				},
				iconTheme: {
					primary: "#ff0033",
					secondary: "#FFFAEE",
				},
			});
		} finally {
			//fetchData();
		}

		router.reload(`/posts/`);
	};

	return (
		<>
			<div className="post-detail">
				<div className="container">
					<div className="col-xs-12 col-sm-12">
						<div className="panel-content">
							{/* <Dropdown>
								<Dropdown.Toggle variant="link" id="dropdown-basic">
									<span className="glyphicon glyphicon-chevron-down"></span>
								</Dropdown.Toggle>
								<Dropdown.Menu>
									<Dropdown.Item href="#">Action</Dropdown.Item>
									<Dropdown.Item href="#">Another action</Dropdown.Item>
									<Dropdown.Item href="#">Something else here</Dropdown.Item>
									<Dropdown.Divider />
									<Dropdown.Item href="#">Separated link</Dropdown.Item>
								</Dropdown.Menu>
							</Dropdown> */}
							<div className="panel-header-content d-flex">
								<div className="panel-user-container d-flex">
									<div className="avatar">
										{author && (<img
											src={
												author.profile_photo
													? author.profile_photo
													: "/images/avatar.png"
											}
											className="rounded-circle flex-shrink-0"
											alt="Author Image"
										/>)
										}
									</div>
									<div className="panel-header-details">
										<h1 className="panel-header-title">
										{/* {post.title} {post.slug} */}
										 {post.slug && <Link href={`/post/${post.slug}`}>{post.title}</Link>}
										</h1>
										<p>{author && (author.username || (`${author.first_name} ${author.last_name}`))}</p>
									</div>
								</div>
								<div className="panel-header-actions">
									{/* <button className="btn btn-primary">Button</button> */}
								</div>
							</div>
							<div className="panel-body">
								{image && <div className="mb-3"><img src={image} alt={post.title} /></div>}
								<p>{short_desc}</p>
								<p>Category:
								{category && (
									<Link
										href={`/category/${category.slug}`}
									>
										<a>{category.name}</a>
									</Link>
								)}</p>
							</div>
							<div className="panel-footer d-flex mb-2 mt-2">
								<Button variant="outline-secondary"
									onClick={() => setFav()}
								>
									{isFav ? <HeartFill/> : <Heart/>} {favCount}
								</Button>
								<Button variant="outline-secondary" disabled>
									<Eye/> {views}
								</Button>
								<Button variant="outline-secondary"
									onClick={() => setOpen(!open)}
									aria-controls="comment-container-collapse"
									aria-expanded={open}
								>
									<Chat/> {commentsCount}
								</Button>
								{/* <Button variant="outline-secondary">
									<Pencil/>
								</Button> */}
								{current_user.id == authorId && (<Button variant="btn btn-danger"
									onClick={() => confirmDelete(id)}
								>
									<Trash/>
								</Button>)}
							</div>
							<Collapse in={open}>
								<div id="comment-container-collapse">
									{id && (<PostCommentsList user={current_user} postId={id} updateCommentsCount={updateCommentsCount}/>) }
								</div>
							</Collapse>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
export default PostsDetailsContent;
