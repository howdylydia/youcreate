import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import baseUrl from "@/utils/baseUrl";
import axios from "axios";
import { useRouter } from "next/router";

const BuyPostBtn = ({ current_user, post }) => {
	const cartItems = useSelector((state) => state.cart.cartItems);
	const discount = useSelector((state) => state.cart.discount);
	const dispatch = useDispatch();
	const [add, setAdd] = useState(false);
	const [alreadyBuy, setAlreadyBuy] = useState(false);
	const router = useRouter();

	useEffect(() => {
		const postExist = cartItems.find((cart) => {
			return post.id === cart.id;
		});
		postExist && setAdd(true);
		if (current_user && post && post.id) {
			const payload = {
				params: { userId: current_user.id, postId: post.id },
			};
			const url = `${baseUrl}/api/posts/post/exist`;
			axios.get(url, payload).then((result) => {
				setAlreadyBuy(result.data.enroll);
			});
		}
	}, [cartItems, post]);

	const buyPost = (postCart) => {
		let postObj = {};
		postObj["id"] = postCart.id;
		postObj["title"] = postCart.title;
		postObj["slug"] = postCart.slug;
		postObj["price"] = discount > 0 ? discount : postCart.latest_price;
		postObj["regular_price"] = postCart.before_price;
		postObj["image"] = postCart.image;
		postObj["lessons"] = postCart.lessons;
		postObj["duration"] = postCart.duration;
		postObj["access_time"] = postCart.access_time;
		postObj["quantity"] = 1;
		postObj[
			"instructor"
		] = `${postCart.user.first_name} ${postCart.user.last_name}`;
		dispatch({
			type: "ADD_TO_CART",
			data: postObj,
		});
		router.push("/checkout");
	};

	return alreadyBuy ? (
		<button
			className="default-btn"
			onClick={() => router.push("/checkout")}
		>
			<i className="flaticon-user"></i> View My Posts <span></span>
		</button>
	) : (
		<>
			{add ? (
				<button
					onClick={() => router.push("/checkout")}
					className="default-btn"
				>
					<i className="flaticon-right-arrow"></i> View Cart
				</button>
			) : (
				<button
					className="default-btn"
					onClick={() => buyPost(post)}
				>
					<i className="flaticon-shopping-cart"></i> Buy Post
					<span></span>
				</button>
			)}
		</>
	);
};

export default BuyPostBtn;
