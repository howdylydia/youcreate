import React from "react";
import Link from "next/link";

const SecurePayment = () => {
	return (
		<>
			<div className="features-area pt-100 pb-70">
				<div className="container">
					<div className="section-title">
						<h2>
                            Easy and secure payment
						</h2>
					</div>

					<div className="row justify-content-center">
						<div className="col-lg-4 col-sm-6 col-md-6">
							<div className="single-features-box">
								<div className="icon">
									<i className="flaticon-brain-process"></i>
								</div>
								<h3>Trusted payment methods</h3>
								<p>
                                Pay with Visa, Mastercard, American Express, Discover, and more
								</p>
							</div>
						</div>

						<div className="col-lg-4 col-sm-6 col-md-6">
							<div className="single-features-box">
								<div className="icon">
									<i className="flaticon-computer"></i>
								</div>
								<h3>Secure payments</h3>
								<p>
                                    Processed in a Level 1 PCI compliant environment
								</p>
							</div>
						</div>

						<div className="col-lg-4 col-sm-6 col-md-6">
							<div className="single-features-box">
								<div className="icon">
									<i className="flaticon-shield-1"></i>
								</div>
								<h3>Payment Processed via Stripe</h3>
								<p>
								Stripe encrypts all customers' credit card numbers and stores decryption information separately, which means Stripe can't see credit card numbers without taking extra steps.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default SecurePayment;
