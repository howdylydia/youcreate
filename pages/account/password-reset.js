import React from "react";
import PasswordResetForm from "@/components/Authentication/PasswordResetForm";
import Navbar from "@/components/_App/Navbar";
import Footer from "@/components/_App/Footer";
import Router, { useRouter } from "next/router";

const ResetPassword = () => {
    const router = useRouter();
    const {token} = router.query;
    return(
        <>
            <Navbar/>
            <div className="bg-f9f9f9 ptb-100">
                <div className="container">
                    <h1>Password Reset</h1>
                    <PasswordResetForm token={token}/>
                </div>
            </div>
            <Footer />
        </>
    );
}

export default ResetPassword;