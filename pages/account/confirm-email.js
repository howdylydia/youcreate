import React from "react";
import axios from "axios";
import Router, { useRouter } from "next/router";
import baseUrl from "@/utils/baseUrl";
import Link from "next/link";
import Navbar from "@/components/_App/Navbar";
import Footer from "@/components/_App/Footer";

const confirmEmail = () => {
	const router = useRouter();

	React.useEffect(() => {
		const confirmEmail = async () => {
			try {
				const { token, email } = router.query;
				const url = `${baseUrl}/api/account/confirm-email`;
				const payload = { token, email };
				await axios.put(url, payload);
				// console.log(response.data);
				setTimeout(() => {
					Router.reload("/");
				}, 2000);
				Router.push("/");
			} catch (error) {
				console.log(error);
			}
		};

		confirmEmail();
	}, [router.query]);

	return (<>
		<Navbar />	
		<div className="subscribe-area bg-f9f9f9 ptb-100">
			<div className="container">
			<h2>Thank you for confirming your email address.</h2>
			<p>Please proceed to finalize your membership.</p>
			<div className="btn-box">
				<Link href="/membership/contribute">
					<a className="default-btn no-icon">
						Become a Member <span></span>
					</a>
				</Link>
			</div>
			</div>
		</div>
		<Footer />
		</>);
};

export default confirmEmail;
