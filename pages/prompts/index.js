import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import PromptsList from "@/components/Prompts/PromptsList";
import Footer from "@/components/_App/Footer";

export default function PromptsPage({ user }) {
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle="Prompts"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Prompts"
			/>
			<PromptsList user={user} />
			<Footer />
		</>
	);
}
