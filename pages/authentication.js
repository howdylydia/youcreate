import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import LoginForm from "@/components/Authentication/LoginForm";
import RegisterForm from "@/components/Authentication/RegisterForm";
import Footer from "@/components/_App/Footer";
import Link from "next/link";

export default function AuthenticationPage({ user }) {
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle="Authentication"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Authentication"
			/>

			<div className="profile-authentication-area ptb-100">
				<div className="container">
					<div className="row">
						<div className="col-lg-6 col-md-12">
							<LoginForm />
						</div>

						<div className="col-lg-6 col-md-12">
							<div className="mb-3">
								<h4>Create your Account</h4>
								<Link href="/membership/register">
									<a className="default-btn no-icon">
										Sign-Up <span></span>
									</a>
								</Link>
							</div>
							<div>
								<h4>Didn't receive a confirmation email?</h4>
								<Link href="/account/send-confirmation-email">
								<a className="default-btn no-icon">
									Request Confirmation
								</a>
								</Link>
							</div>
						</div>
					</div>
				</div>
			</div>

			<Footer />
		</>
	);
}
