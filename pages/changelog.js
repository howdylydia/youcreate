import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import Footer from "@/components/_App/Footer";

export default function ChangeLogPage(user) {
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle="Changelog"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Changelog"
			/>

			<div className="privacy-policy-area ptb-100">
				<div className="container">
					<div className="row">
						<div className="col-lg-12 col-md-12">
							<div>
								<h2>Oct 8, 2023</h2>
								<ul>
									<li>
										<span className="badge text-bg-primary">New</span> Users can delete their post comments
									</li>
								</ul>
							</div>
							<div>
								<h2>Sept 27, 2023</h2>
								<ul>
									<li>
										<span className="badge text-bg-secondary">Improved</span> working donation form & membership
									</li>
								</ul>
							</div>
							<div>
								<h2>Sept 14, 2023</h2>
								<ul>
									<li>
										<span className="badge text-bg-primary">New</span> Submit posts to prompts
									</li>
								</ul>
							</div>
							<div>
								<h2>Sept 13, 2023</h2>
								<ul>
									<li>
										<span className="badge text-bg-primary">New</span> Add post comments, Favourite/unfavorite posts, Delete post
									</li>
								</ul>
							</div>
							<div>
								<h2>Sept 11, 2023</h2>
								<ul>
									<li>
										<span className="badge text-bg-secondary">Improved</span> Create Posts: automatically resize image files
									</li>
								</ul>
							</div>
							<div>
								<h2>Sept 6, 2023</h2>
								<ul>
									<li>
										<span className="badge text-bg-primary">New</span> Create Posts
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<Footer />
		</>
	);
}
