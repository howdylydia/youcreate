import { Op } from "sequelize";
import { Prompt, User, Category } from "database/models";

export default async function handler(req, res) {
	switch (req.method) {
		case "GET":
			await handleGetRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGetRequest = async (req, res) => {
	const { page, limit, short, search } = req.query;
	const pageNumber = parseInt(page);
	const getRealNumber = isNaN(pageNumber) ? 0 : pageNumber;
	const promptsOffset = limit * (getRealNumber - 1);
	const LIMIT = parseInt(limit);
    // console.log("post: ", Post);

    // res.status(200).json("post test");

	try {
		let totalPages;
		totalPages = await Prompt.count();

		const prompts = await Prompt.findAll({
			include: [
				{
					model: Category,
					as: "category",
					attributes: ["name", "slug"],
				},
			],
			where: {
				[Op.or]: [
					{ title: { [Op.like]: `%${search}%` } },
					{ short_desc: { [Op.like]: `%${search}%` } },
				]
			},
			order: [["created_at", "DESC"]],
			offset: promptsOffset,
			limit: LIMIT,
		});

		const promptsCount = await Prompt.count({
			where: {
				[Op.or]: [
					{ title: { [Op.like]: `%${search}%` } },
					{ short_desc: { [Op.like]: `%${search}%` } },
				]
			},
		});

		totalPages = Math.ceil(totalPages / limit);


    //res.status(200).json("post test");
		res.status(200).json({
			prompts,
			totalPages,
			promptsCount,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "get_all_prompts",
			message: e.message,
		});
	}
};
