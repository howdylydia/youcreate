import bcrypt from "bcrypt";
import { v4 as uuidv4 } from "uuid";
import jwt from "jsonwebtoken";
import isEmail from "validator/lib/isEmail";
import isLength from "validator/lib/isLength";

import User from "database/models/user";

import { confirmEmailAddress } from "email-templates/account-confirmation";

export default async function handler(req, res) {
	switch (req.method) {
		case "POST":
			await userSignup(req, res);
			break;
		case "PUT": // Add a new case for password reset
			await resetPassword(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const userSignup = async (req, res) => {
	const confirmToken = uuidv4();
	let { first_name, last_name, username, email, password } = req.body;
	try {
		if (!isLength(first_name, { min: 3 })) {
			return res.status(422).json({
				message:
					"The first name should be a minimum of three characters long",
			});
		} else if (!isLength(last_name, { min: 3 })) {
			return res.status(422).json({
				message:
					"The last name should be a minimum of three characters long",
			});
		} else if (!isEmail(email)) {
			return res
				.status(422)
				.json({ message: "Email should be a valid email address" });
		} else if (!isLength(username, { min: 4 })) {
			return res.status(422).json({
				message:
					"The username should be a minimum of four characters long",
			});
		} else if (!isLength(password, { min: 6 })) {
			return res.status(422).json({
				message: "Password should be minimum of six characters long",
			});
		}

		// check if the user with that username already exists 
		const userWithUsername = await User.findOne({
			where: { username: username}
		});

		if (userWithUsername) {
			return res
				.status(422)
				.json({ message: `User already exist with username ${username}` });
		}

		// Check if user with that email if already exists
		const user = await User.findOne({
			where: { email: email },
		});

		if (user) {
			return res
				.status(422)
				.json({ message: `User already exist with email ${email}` });
		}

		// Encrypt password with bcrypt
		const passwordHash = await bcrypt.hash(password, 10);

		const newUser = await User.create({
			first_name,
			last_name,
			username,
			email,
			password: passwordHash,
			reset_password_token: confirmToken,
			reset_password_send_at: Date.now(),
		});

		confirmEmailAddress(newUser, 'signup', req, res);

		const elarniv_users_token = jwt.sign(
			{
				userId: newUser.id,
				first_name: newUser.first_name,
				last_name: newUser.last_name,
				username: newUser.username,
				email: newUser.email,
				role: newUser.role,
				profile_photo: newUser.profile_photo,
			},
			process.env.JWT_SECRET,
			{
				expiresIn: "7d",
			}
		);

		res.status(200).json({
			message: "Registration Successful!",
			elarniv_users_token,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "create_user",
			message: e.message,
		});
	}
};

const resetPassword = async (req, res) => {
	const { email, newPassword } = req.body;
  
	try {
	  // Check if the user with the provided email exists
	  const user = await User.findOne({
		where: { email },
	  });
  
	  if (!user) {
		return res.status(404).json({ message: "User not found." });
	  }
  
	  // Encrypt the new password with bcrypt
	  const passwordHash = await bcrypt.hash(newPassword, 10);
  
	  // Update the user's password
	  user.password = passwordHash;
  
	  await user.save();
  
	  res.status(200).json({ message: "Password reset successful." });
	} catch (e) {
	  res.status(400).json({
		error_code: "reset_password",
		message: e.message,
	  });
	}
  };