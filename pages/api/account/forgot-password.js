/* API: send an email to the user with a token */
import User from "database/models/user";
import { v4 as uuidv4 } from "uuid";
import isEmail from "validator/lib/isEmail";
import { forgotPassword } from "@/email-templates/forgot-password-email";

export default async function handler(req, res) {
	switch (req.method) {
		case "POST":
			await passwordResetRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const passwordResetRequest = async (req, res) => {
	let { email } = req.body;
	try {
		if (!isEmail(email)) {
			return res.status(422).json({
				message: "Email should be a valid email address",
			});
		}

		// Check if user with that email if already exists
		const user = await User.findOne({
			where: { email: email },
		});

		if (user) {
			// Generate a reset token
			const resetToken = uuidv4();

			// Save the reset token and its expiration time in the database
			user.reset_password_token = resetToken;
			// user.reset_password_expires = Date.now() + 3600000; // Token valid for 1 hour
			await user.save();
			console.log("reset token ==== ", resetToken);
			forgotPassword(user, req, res);

			// res.status(200).json({
			// 	message: "Please check your email and confirm.",
			// });
		} else {
			res.status(422).json({
				message:
					"This Email does not exist! Please check if this is the correct email.",
			});
		}
	} catch (e) {
		res.status(400).json({
			error_code: "confirm_email",
			message: e.message,
		});
	}
};


// export default async function handler(req, res) {
//     if (req.method === "POST") {
//       try {
//         const { email } = req.body;
//         // Check if the user with this email exists
//         const user = await User.findOne({ where: { email } });
  
//         if (!user) {
//           return res.status(404).json({ message: "User not found" });
//         }
  
//         // Generate a reset token (you can use a library like `uuid`)
//         const resetToken = uuidv4();
  
//         // Save the reset token and its expiration time in the database
//         user.reset_password_token = resetToken;
//         user.reset_password_expires = Date.now() + 3600000; // Token valid for 1 hour
//         await user.save();
  
//         // Send an email with a link to the reset password form
//         const resetPasswordLink = `${process.env.BASE_URL}/reset-password?token=${resetToken}`;
//         await sendPasswordResetEmail(email, resetPasswordLink);
  
//         res.status(200).json({ message: "Password reset email sent" });
//       } catch (error) {
//         res.status(500).json({ error: "Error processing request" });
//       }
//     } else {
//       res.status(405).json({ message: `Method ${req.method} not allowed` });
//     }
//   }