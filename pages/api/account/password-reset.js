import bcrypt from "bcrypt";
import User from "database/models/user";
// TO-DO: add reset_password_expires

export default async function handler(req, res) {
  if (req.method === "POST") {
    try {
      const { token, password } = req.body;

      console.log("token, password===", token, password);
      // Check if the token is valid and not expired
      const user = await User.findOne({
        // where: { reset_password_token: token, reset_password_expires: { $gt: Date.now() } },
        where: { reset_password_token: token },
      });

      console.log("Found user:", user, user.reset_password_token );

      if (!user) {
        return res.status(400).json({ message: "Invalid or expired token" });
      }

      // Encrypt the new password with bcrypt
      const passwordHash = await bcrypt.hash(password, 10);

      console.log("Password hashed successfully:", passwordHash);

      // Update the user's password and reset the token
      user.password = passwordHash;
      // user.reset_password_token = null;
      // user.reset_password_expires = null;
      await user.save();

      console.log("password reset successfull", password, passwordHash);
      res.status(200).json({ message: "Password reset successful" });
    } catch (error) {
      res.status(500).json({ error: "Error processing request" });
    }
  } else {
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  }
}