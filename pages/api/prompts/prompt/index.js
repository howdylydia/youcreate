import { Prompt, User, Category } from "database/models";

export default async function handler(req, res) {
	switch (req.method) {
		case "GET":
			await handleGetRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGetRequest = async (req, res) => {
	const { slug } = req.query;
	try {
		const prompt = await Prompt.findOne({
			include: [
				{
					model: Category,
					as: "category",
					attributes: ["name", "slug"],
				}
			],
			where: { slug: slug },
		});

		res.status(200).json({
			prompt,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "get_prompt",
			message: e.message,
		});
	}
};
