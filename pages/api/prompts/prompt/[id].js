import Prompt from "database/models/prompt";

export default async function handler(req, res) {
	if (!("authorization" in req.headers)) {
		return res.status(401).json({ message: "No autorization token" });
	}
	switch (req.method) {
		case "GET":
			await handleGet(req, res);
			break;
		case "PUT":
			await handlePut(req, res);
			break;
		case "DELETE":
			await handleDelete(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGet = async (req, res) => {
	const { id } = req.query;
	try {
		const prompt = await Prompt.findOne({
			where: { id: id },
		});

		res.status(200).json({ prompt });
	} catch (e) {
		res.status(400).json({
			error_code: "update_prompt",
			message: e.message,
		});
	}
};

const handlePut = async (req, res) => {
	const { id } = req.query;
	const {
		title,
		short_desc,
		image,
		catId,
	} = req.body;
	try {
		const prompt = await Prompt.update(
			{
				title,
				short_desc,
				image,
				catId,
			},
			{
				where: { id: id },
			}
		);

		res.status(200).json({ message: "Prompt updated successfully" });
	} catch (e) {
		res.status(400).json({
			error_code: "update_prompt",
			message: e.message,
		});
	}
};

const handleDelete = async (req, res) => {
	const { id } = req.query;
	try {
		const prompt = await Prompt.findOne({
			where: { id: id },
		});

		prompt.destroy();

		res.status(200).json({ message: "Prompt deleted successfully" });
	} catch (e) {
		res.status(400).json({
			error_code: "delete_prompt",
			message: e.message,
		});
	}
};
