import { Post_Comment, Post } from "database/models";
import jwt from "jsonwebtoken";

export default async function handler(req, res) {
	switch (req.method) {
        case "POST":
            await handlePostRequest(req, res);
            break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handlePostRequest = async (req, res) => {
	const { comment, postId } = req.body;

	try {
		const { userId } = jwt.verify(
			req.headers.authorization,
			process.env.JWT_SECRET
		);
        
        const newComment = await Post_Comment.create({
            comment, 
            postId,
			userId
        });

		// Retrieve the post from the database
		const post = await Post.findByPk(postId);

		if (!post) {
		return res.status(404).json({ error: "Post not found" });
		}

		// Increment the view count
		post.post_comments_count += 1;
		await post.save();

        res.status(200).json({message: "Successfully posted a comment.", newComment});
	} catch (e) {
		res.status(400).json({
			error_code: "post_new_comment",
			message: e.message,
		});
	}
};
