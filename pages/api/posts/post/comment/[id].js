import { Post_Comment, Post } from "@/database/models";

export default async function handler(req, res) {
    if(!("authorization" in req.headers)) {
        return res.status(401).json({ message: "No authorization token"});
    }
    switch(req.method) {
        case "GET":
            break;
        case "PUT":
            break;
        case "DELETE":
            await handleDelete(req, res);
            break;
        default:   
            res.status(405).json(
                {message:`Method ${req.method} not allowed`}
            );
    }
}

const handleDelete = async (req, res) => {
    console.log("handle comenmt delete", req.query);
    const {id} = req.query;
    try {
        const comment = await Post_Comment.findByPk(id);
        comment.destroy();

        const post = await Post.findByPk(comment.postId);

        // decrement post comments count
        post.post_comments_count -= 1;
        await post.save();

        res.status(200).json({message: "Comment deleted successfully."})
    } catch (e) {
        res.status(400).json({
            error_code: "delete_post_comment",
            message: e.message
        });
    }
}