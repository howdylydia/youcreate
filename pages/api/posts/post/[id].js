import Post from "database/models/post";
import { deleteFromS3 } from "@/utils/awsHelper";

export default async function handler(req, res) {
	if (!("authorization" in req.headers)) {
		return res.status(401).json({ message: "No autorization token" });
	}
	switch (req.method) {
		case "GET":
			await handleGet(req, res);
			break;
		case "PUT":
			await handlePut(req, res);
			break;
		case "DELETE":
			await handleDelete(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGet = async (req, res) => {
	const { id } = req.query;
	try {
		const post = await Post.findOne({
			where: { id: id },
		});

		res.status(200).json({ post });
	} catch (e) {
		res.status(400).json({
			error_code: "update_post",
			message: e.message,
		});
	}
};

const handlePut = async (req, res) => {
	const { id } = req.query;
	const {
		title,
		short_desc,
		image,
		catId,
	} = req.body;
	try {
		const post = await Post.update(
			{
				title,
				short_desc,
				image,
				catId,
			},
			{
				where: { id: id },
			}
		);

		res.status(200).json({ message: "Post updated successfully" });
	} catch (e) {
		res.status(400).json({
			error_code: "update_post",
			message: e.message,
		});
	}
};

const handleDelete = async (req, res) => {
	const { id } = req.query;
	try {
		const post = await Post.findOne({
			where: { id: id },
		});
		if (post.image) {
			deleteFromS3(post.image)
				.then(() => {
					console.log("Deleted " + post.title + " successuflly!");
				})
				.catch((err) => {
					console.error("Error deleting " + post.title, err);
				});
		}
		post.destroy();
		res.status(200).json({ message: "Post deleted successfully" });
	} catch (e) {
		res.status(400).json({
			error_code: "delete_post",
			message: e.message,
		});
	}
};
