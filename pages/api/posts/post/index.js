import { Post, User, Category } from "database/models";

export default async function handler(req, res) {
	switch (req.method) {
		case "GET":
			await handleGetRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGetRequest = async (req, res) => {
	const { slug } = req.query;
	try {
		const post = await Post.findOne({
			include: [
				{
					model: User,
					as: "user",
					attributes: [
						"first_name",
						"last_name",
						"username",
						"profile_photo",
						"bio",
					],
				},
				{
					model: Category,
					as: "category",
					attributes: ["name", "slug"],
				}
			],
			where: { slug: slug },
		});

		res.status(200).json({
			post,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "get_post",
			message: e.message,
		});
	}
};
