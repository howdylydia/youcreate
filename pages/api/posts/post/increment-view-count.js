import Post from "database/models/post";

export default async function handler(req, res) {

    const postId = req.query && req.query.postId || req.body && req.body.postId;
	try {
        
         // Retrieve the post from the database
        const post = await Post.findByPk(postId);

        if (!post) {
        return res.status(404).json({ error: "Post not found" });
        }

        // Increment the view count
        post.views += 1;
        await post.save();

		res.json({ message: "View count incremented successfully" });
	} catch (e) {
		res.status(400).json({
			error_code: "update_post_view_count",
			message: e.message,
		});
	}
}