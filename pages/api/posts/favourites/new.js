import { Post, Post_Favourite } from "database/models";

export default async function handler(req, res) {
	switch (req.method) {
		case "GET":
			await handleGet(req, res);
			break;
		case "POST":
			await handlePost(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGet = async (req, res) => {
	const { userId, postId } = req.query;

	try {
		const exist = await Post_Favourite.findOne({
			where: { userId, postId },
		});

		if (exist) {
			res.status(200).send(true);
		} else {
			res.status(200).send(false);
		}
	} catch (e) {
		res.status(400).json({
			error_code: "make_post_favourite",
			message: e.message,
		});
	}
};

const handlePost = async (req, res) => {
	const { userId, postId } = req.body;
	console.log("favorites post");
	try {
		const favexist = await Post_Favourite.findOne({
			where: { userId, postId }
		});
		const post = await Post.findByPk(postId);

		if (!favexist) {
			console.log("create fav");
			// if it's not favorited 
			await Post_Favourite.create({
				userId,
				postId
			});
			post.favourites_count += 1; // increase post count
			await post.save();
			res.status(200).json({
				message: "Added to post favourites", 
				status: true, 
				favourites_count: post.favourites_count
			});
		} else {
			favexist.destroy();
			post.favourites_count -= 1; // decrease post count
			await post.save();
			res.status(200).json({
				message: "Removed from post favourites", 
				status: false, 
				favourites_count: post.favourites_count
			});
		}
	} catch (e) {
		res.status(400).json({
			error_code: "make_post_favourites",
			message: e.message,
		});
	}
};
