import jwt from "jsonwebtoken";
import { slugify } from "@/utils/auth";
import Post from "database/models/post";
import formidable from "formidable";
import sharp from "sharp";
import { uploadToS3 } from "@/utils/awsHelper";
import fs from 'fs';

// Must disable the Next.js default parser to use formidable 
export const config = {
	api: {
	  bodyParser: false,
	},
  };

export default async function handler(req, res) {
	if (!("authorization" in req.headers)) {
		return res.status(401).json({ message: "No autorization token" });
	}
	switch (req.method) {
		case "POST":
			await handlePostRequest(req, res);
			break;
		case "DELETE":
			await handleDeleteRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handlePostRequest = async (req, res) => {	
	try {
		const { userId } = jwt.verify(
			req.headers.authorization,
			process.env.JWT_SECRET
		);
		let image; 
		
		const form = formidable({multiples: true});
		const result = await new Promise(function(resolve, reject) {
			form.parse(req, async function (err, fields, files) {
				if (err) {
					reject(err)
				}
				console.log('parsing.....', files);
				if(files && files.image) {
					image = files && files.image && files.image[0];
				}
				resolve({files, fields});
			});
		})
		console.log('after parse');		
		
		const title = result.fields.title[0];
		const short_desc = result.fields.short_desc[0];
		const catId = result.fields.catId[0];
		const promptId = result.fields.promptId[0];

		let slug = slugify(title);
		const slugExist = await Post.findOne({
			where: { slug: slug },
		});

		if (slugExist) {
			slug = `${slug}-${Math.floor(
				Math.random() * (999 - 100 + 1) + 100
			)}`;
		}

		if(image) {
			// Read the contents of the uploaded image file
			const imageBuffer = fs.readFileSync(image && image.filepath);

			// Resize the image buffer using Sharp
			const resizedImage = await sharp(imageBuffer)
				.resize({ width: 800, height: 800, fit: 'inside', })
				.jpeg()
				.toBuffer();

			await uploadToS3(resizedImage, slug + ".jpeg")
				.then(link => {
					image = link;
					console.log("image link", link);
				});
		}

		const newpost = await Post.create({
			title,
			slug,
			short_desc,
			image,
			userId,
			catId,
			promptId
		});

		res.status(200).json({
			message:
				"Post created successfully.",
			post: newpost,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "create_post",
			message: e.message,
		});
	}
};

// View: https://codesandbox.io/s/nextjs-simple-upload-file-to-server-thyb0?file=/pages/index.js:636-720