import Stripe from "stripe";
import jwt from "jsonwebtoken";

// This is your test secret API key.
const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);

// const calculateTax = async (items, currency) => {
//   const taxCalculation = await stripe.tax.calculations.create({
//     currency,
//     customer_details: {
//       address: {
//         line1: "920 5th Ave",
//         city: "Seattle",
//         state: "WA",
//         postal_code: "98104",
//         country: "US",
//       },
//       address_source: "shipping",
//     },
//     line_items: items.map((item) => buildLineItem(item)),
//   });

//   return taxCalculation;
// };

const buildLineItem = (item) => {
  return {
    amount: item.amount, // Amount in cents
    reference: item.id, // Unique reference for the item in the scope of the calculation
  };
};

// Securely calculate the order amount, including tax
const calculateOrderAmount = (items, taxCalculation) => {
  // Replace this constant with a calculation of the order's amount
  // Calculate the order total with any exclusive taxes on the server to prevent
  // people from directly manipulating the amount on the client
  let orderAmount = 1400;
  orderAmount += taxCalculation.tax_amount_exclusive;
  return orderAmount;
};

export default async function handler(req, res) {
  const { userId } = jwt.verify(
    req.headers.authorization,
    process.env.JWT_SECRET
  );
  const { items } = req.body;
//   taxCalculation = calculateTax(items);

  // Create a PaymentIntent with the order amount and currency
  const paymentIntent = await stripe.paymentIntents.create({
    amount: items[0].amount,
    currency: "USD",
    // In the latest version of the API, specifying the `automatic_payment_methods` parameter is optional because Stripe enables its functionality by default.
    automatic_payment_methods: {
      enabled: true,
    },
    metadata: {
      userId: userId,
      membership_type: 'basic',
      //tax_calculation: taxCalculation.id
    },
  });

  res.send({
    clientSecret: paymentIntent.client_secret,
  });

  // Invoked when `payment_intent.succeeded` webhook is received
  const handlePaymentIntentSucceeded = async (paymentIntent) => {
    // Create a Tax Transaction for the successful payment
    stripe.tax.transactions.createFromCalculation({
      calculation: paymentIntent.metadata['tax_calculation'],
      reference: 'myOrder_123', // Replace with a unique reference from your checkout/order system
    });
  };
};

// import Stripe from "stripe";

// const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);

// const buildLineItem = (item) => {
//   return {
//     amount: item.amount, // Amount in cents
//     reference: item.id, // Unique reference for the item in the scope of the calculation
//   };
// };

// const calculateOrderAmount = (items) => {
//   // Calculate the order total based on your business logic, including taxes if applicable
//   let orderAmount = 0;
//   for (const item of items) {
//     orderAmount += item.amount;
//   }
//   return orderAmount;
// };

// export default async function handler(req, res) {
//   const { items } = req.body;

//   // Calculate the order amount based on the items
//   const orderAmount = calculateOrderAmount(items);

//   try {
//     // Create a PaymentIntent with the calculated order amount
//     const paymentIntent = await stripe.paymentIntents.create({
//       amount: orderAmount,
//       currency: "USD",
//     });

//     res.status(200).json({
//       clientSecret: paymentIntent.client_secret,
//     });
//   } catch (error) {
//     console.error("Error creating PaymentIntent:", error);
//     res.status(500).json({ error: "Unable to create PaymentIntent" });
//   }
// }