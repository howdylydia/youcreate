import { Op } from "sequelize";
import { Post_Comment, User } from "database/models";

export default async function handler(req, res) {
	switch (req.method) {
		case "GET":
			await handleGetRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGetRequest = async (req, res) => {
	const { page, limit, postId } = req.query;
	const pageNumber = parseInt(page);
	const getRealNumber = isNaN(pageNumber) ? 0 : pageNumber;
	const post_commentsOffset = limit * (getRealNumber - 1);
	const LIMIT = parseInt(limit);
    // console.log("post: ", Post);

    // res.status(200).json("post test");

	try {
		let totalPages;
		//totalPages = await Post_Comment.count();

		const post_comments = await Post_Comment.findAll({
			where: {
				postId: postId
			},
			order: [["created_at", "DESC"]],
			include: [
				{
					model: User,
					as: "user",
					attributes: ["id", "first_name", "last_name", "profile_photo"],
				}
			],
			offset: post_commentsOffset,
			limit: LIMIT,
		});

		const post_commentsCount = await Post_Comment.count({
			where: {
				postId: postId
			},
		});

		totalPages = Math.ceil(post_commentsCount / limit);

    //res.status(200).json("post test");
		res.status(200).json({
			post_comments,
			totalPages,
			post_commentsCount,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "get_all_post_comments",
			message: e.message,
		});
	}
};
