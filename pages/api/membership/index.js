import jwt from "jsonwebtoken";
import { Membership } from "database/models";

export default async function handler(req, res) {
	if (!("authorization" in req.headers)) {
		return res.status(401).json({ message: "No autorization token" });
	}
	switch (req.method) {
		case "GET":
			await handleGetRequest(req, res);
			break;
		case "POST":
			await handlePostRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handlePostRequest = async (req, res) => {
	const { 
		membership_type,
		total_paid,
		method 
	} = req.body;
	const start_at = new Date();
	const end_at = new Date(start_at);
	end_at.setFullYear(end_at.getFullYear()-1);
	
	try {
		const { userId } = jwt.verify(
			req.headers.authorization,
			process.env.JWT_SECRET
		);
		
		let membership; 

		membership = await Membership.findOne({
			where: { userId: userId },
		});

		if (membership) {
			membership.membership_type = membership_type;
			membership.total_paid = total_paid;
			membership.method = method;
			membership.payment_status = "paid";
			membership.start_at = start_at;
			membership.end_at = end_at;
			await membership.save();
		} else {
			membership = await Membership.create({
				membership_type,
				total_paid,
				method,
				userId,
				payment_status: "paid",
				start_at,
				end_at
			});
		}

		res.status(200).json({
			message:
				"Membership created successfully.",
			membership: membership,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "create_my_membership",
			message: e.message,
		});
	}
};

const handleGetRequest = async (req, res) => {
	try {
		const { userId } = jwt.verify(
			req.headers.authorization,
			process.env.JWT_SECRET
		);

		const membership = await Membership.findOne({
			where: { userId: userId },
		});

		res.status(200).json({
			membership
		});
	} catch (e) {
		res.status(400).json({
			error_code: "get_my_membership",
			message: e.message,
		});
	}
};
