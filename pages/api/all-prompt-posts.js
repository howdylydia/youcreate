import { Op } from "sequelize";
import { Post, User, Prompt } from "database/models";

export default async function handler(req, res) {
	switch (req.method) {
		case "GET":
			await handleGetRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGetRequest = async (req, res) => {
	const { page, limit, promptId } = req.query;
	const pageNumber = parseInt(page);
	const getRealNumber = isNaN(pageNumber) ? 0 : pageNumber;
	const postsOffset = limit * (getRealNumber - 1);
	const LIMIT = parseInt(limit);
    // console.log("post: ", Post);

    // res.status(200).json("post test");

	try {
		let totalPages;
		totalPages = await Post.count();


		const posts = await Post.findAll({
			where: {
				promptId: promptId
			},
			order: [["created_at", "DESC"]],
			include: [
				{
					model: User,
					as: "user",
					attributes: ["first_name", "last_name", "profile_photo"],
				}
			],
			offset: postsOffset,
			limit: LIMIT,
		});

		const postsCount = await Post.count({
			where: {
				promptId: promptId
			},
		});

		totalPages = Math.ceil(totalPages / limit);

    //res.status(200).json("post test");
		res.status(200).json({
			posts,
			totalPages,
			postsCount,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "get_all_prompt_posts",
			message: e.message,
		});
	}
};
