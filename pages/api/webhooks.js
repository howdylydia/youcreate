import Stripe from 'stripe';
import { buffer } from 'micro';
import Membership from '@/database/models/membership';
import { start } from 'nprogress';

export const config = {
    api: {
        bodyParser: false,
    }
};

export default async function handler(req, res) {
	switch (req.method) {
		case "POST":
			await handlePostRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handlePostRequest = async (req, res) => {	
    const stripe = new Stripe(process.env.STRIPE_SECRET_KEY);
    //const { userId, clientSecret } = req.body;

 
        const buf = await buffer(req);
        const sig = req.headers['stripe-signature'];
        const webhookSecret = process.env.STRIPE_WEBHOOK_SIGNING_SECRET;

        let event;
        try {
            // if signature doesn't exist - didn't come from stripe
            if (!sig || !webhookSecret) {
                return;
            }

            event = stripe.webhooks.constructEvent(buf, sig, webhookSecret);
        
            switch(event.type) {
                case "payment_intent.succeeded":
                /* Upon successful payment, create the membership record or update */
                const paymentIntentSucceeded = event.data.object;
                const {
                    metadata: { userId, membership_type },
                    amount,
                  } = paymentIntentSucceeded;                
                const total_paid = paymentIntentSucceeded.amount;
                const method = paymentIntentSucceeded.payment_method_types[0];

                await createOrUpdateMembership(userId, membership_type, amount, method);
                break;
            default: 
                console.log(`Unhandled event type ${event.type}`);
        }
        // Respond with a 200 OK to acknowledge receipt of the event
        res.json({ received: true });
    } catch (error) {
        console.log(`Webhook error: ${error.message}`);
        return res.status(400).send(`Webhook error: ${error.message}`);
    }
}
// https://stripe.com/docs/api/metadata


// Function to create or update a Membership
const createOrUpdateMembership = async (userId, membership_type, amount, method) => {
    try {
        const start_at = new Date();
	    const end_at = new Date(start_at);
	    end_at.setFullYear(end_at.getFullYear()+1);

        // Check if the user already has a Membership
        let membership = await Membership.findOne({
            where: { userId },
        });
        
        if (membership) {
            // Update existing Membership
            membership.total_paid += amount;
            membership.membership_type = membership_type;
            membership.method = method;
            membership.payment_status = "paid";
            membership.end_at = end_at;
            // Update other Membership properties as needed
            await membership.save();
        } else {
            // Create a new Membership
            membership = await Membership.create({
                userId,
                total_paid: amount,
                membership_type,
                method,
                payment_status: "paid",
                start_at,
                end_at
            });
        }

      console.log(`Membership created/updated for user ${userId}`);
    } catch (error) {
      console.error(`Error creating/updating Membership: ${error.message}`);
    }
  };