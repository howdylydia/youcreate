import jwt from "jsonwebtoken";
import { Contest, User } from "database/models";

export default async function handler(req, res) {
	if (!("authorization" in req.headers)) {
		return res.status(401).json({ message: "No autorization token" });
	}
	switch (req.method) {
		case "GET":
			await handleGetRequest(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGetRequest = async (req, res) => {
	try {
		const contests = await Contest.findAll({
			order: [["created_at", "DESC"]]
		});

		res.status(200).json({
			contests,
		});
	} catch (e) {
		res.status(400).json({
			error_code: "get_my_contests",
			message: e.message,
		});
	}
};
