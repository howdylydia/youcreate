import Contest from "database/models/contest";

export default async function handler(req, res) {
	switch (req.method) {
		case "GET":
			await handleGet(req, res);
			break;
		case "PUT":
			await handlePut(req, res);
			break;
		case "DELETE":
			await handleDelete(req, res);
			break;
		default:
			res.status(405).json({
				message: `Method ${req.method} not allowed`,
			});
	}
}

const handleGet = async (req, res) => {
	const { id } = req.query;
	try {
		const contest = await Contest.findOne({
			where: { id: id },
		});

		res.status(200).json({ contest });
	} catch (e) {
		res.status(400).json({
			error_code: "update_contest",
			message: e.message,
		});
	}
};

const handlePut = async (req, res) => {
	if (!("authorization" in req.headers)) {
		return res.status(401).json({ message: "No autorization token" });
	}

	const { id } = req.query;
	const {
		title,
		short_desc,
		image,
		catId,
	} = req.body;
	try {
		const contest = await Contest.update(
			{
				title,
				short_desc,
				image,
				catId,
			},
			{
				where: { id: id },
			}
		);

		res.status(200).json({ message: "Contest updated successfully" });
	} catch (e) {
		res.status(400).json({
			error_code: "update_contest",
			message: e.message,
		});
	}
};

const handleDelete = async (req, res) => {
	if (!("authorization" in req.headers)) {
		return res.status(401).json({ message: "No autorization token" });
	}
	
	const { id } = req.query;
	try {
		const contest = await Contest.findOne({
			where: { id: id },
		});

		contest.destroy();

		res.status(200).json({ message: "Contest deleted successfully" });
	} catch (e) {
		res.status(400).json({
			error_code: "delete_contest",
			message: e.message,
		});
	}
};
