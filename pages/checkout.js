import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
// import CheckoutForm from "@/components/Checkout/CheckoutForm";
import StripePaymentContainer from "@/components/Stripe/StripePaymentContainer";
import Footer from "@/components/_App/Footer";
import SecurePayment from "@/components/Checkout/SecurePayment";
import { useRouter } from 'next/router'

export default function CheckoutPage({ user }) {
	const router = useRouter();
	let itemList = [{"id":"","amount":""}];
	if (router.query && router.query.itemList) {
		itemList = JSON.parse(router.query.itemList);
	}
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle="Checkout"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Checkout"
			/>
			<button type="button" onClick={() => router.back()}>
				Go back
			</button> 
			<StripePaymentContainer user={user} itemList={itemList}/>

			<SecurePayment />
			<Footer />
		</>
	);
}
