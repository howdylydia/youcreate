import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import EventsList from "@/components/Events/EventsList";
import Footer from "@/components/_App/Footer";

export default function EventsPage({ user }) {
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle="Events"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Events"
			/>
			<EventsList user={user} />
			<Footer />
		</>
	);
}
