import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import ContestList from "@/components/Contests/ContestList";
import Footer from "@/components/_App/Footer";

export default function ContestsPage({ user }) {
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle="Contests"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Contests"
			/>
			<ContestList user={user} />
			<Footer />
		</>
	);
}
