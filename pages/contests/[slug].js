import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import SingleContestDetail from "@/components/Contests/SingleContest/SingleContestDetail";
import Footer from "@/components/_App/Footer";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";

export default function SingleContestsPage({ user }) {
	const [contest, setContest] = useState({});
	const router = useRouter();
	const { slug } = router.query;
	
	useEffect(() => {
		const fetchContest = async () => {
			try {
				const payload = {
					params: { slug: slug },
				};
				const url = `${baseUrl}/api/contests/contest/`;
				const response = await axios.get(url, payload);
				setContest(response.data.contest);
			} catch (err) {
				let message = "An error occurred, but the response structure is unexpected.";
				// Check if err.response and err.response.data exist before destructuring
				if (err.response && err.response.data && err.response.data.message) {
					message = err.response.data.message || err.response.data.error.message;
				}
				toast.error(message, {
					style: {
						border: "1px solid #ff0033",
						padding: "16px",
						color: "#ff0033",
					},
					iconTheme: {
						primary: "#ff0033",
						secondary: "#FFFAEE",
					},
				});
			}
		};

		fetchContest();
	}, [slug]);
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle={contest && contest.title}
				homePageUrl="/contests"
				homePageText="Contests"
				activePageText={contest && contest.title}
			/>
			{contest && <SingleContestDetail user={user} contest={contest} />}
            {/* {contest && <ContestPostsList user={user} contest={contest} /> } */}
			<Footer />
		</>
	);
}
