import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import PostsList from "@/components/Posts/PostsList";
import Footer from "@/components/_App/Footer";

export default function PostsPage({ user }) {
	return (
		<>
			<Navbar user={user} />

			{/* <PageBanner
				pageTitle="Posts"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Posts"
			/> */}
			<PostsList user={user} />
			<Footer />
		</>
	);
}
