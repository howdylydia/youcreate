import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import PromptsDetailsContent from "@/components/SinglePrompts/PromptsDetailsContent";
import Footer from "@/components/_App/Footer";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";
import PromptPostsList from "@/components/Posts/PromptPostsList";

export default function SinglePromptsPage({ user }) {
	const [prompt, setPrompt] = useState({});
	const router = useRouter();
	const { slug } = router.query;
	
	useEffect(() => {
		const fetchPrompt = async () => {
			try {
				const payload = {
					params: { slug: slug },
				};
				const url = `${baseUrl}/api/prompts/prompt`;
				const response = await axios.get(url, payload);
				setPrompt(response.data.prompt);
			} catch (err) {
				let message = "An error occurred, but the response structure is unexpected.";
				// Check if err.response and err.response.data exist before destructuring
				if (err.response && err.response.data && err.response.data.message) {
					message = err.response.data.message || err.response.data.error.message;
				}
				toast.error(message, {
					style: {
						border: "1px solid #ff0033",
						padding: "16px",
						color: "#ff0033",
					},
					iconTheme: {
						primary: "#ff0033",
						secondary: "#FFFAEE",
					},
				});
			}
		};

		fetchPrompt();
	}, [slug]);
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle={prompt && prompt.title}
				homePageUrl="/prompts"
				homePageText="Prompts"
				activePageText={prompt && prompt.title}
			/>
			{prompt && <PromptsDetailsContent user={user} prompt={prompt} />}
            {prompt && <PromptPostsList user={user} prompt={prompt} /> }
			<Footer />
		</>
	);
}
