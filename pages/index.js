import React from "react";
import Navbar from "@/components/_App/Navbar";
import MainBanner from "@/components/eLearningSchool/MainBanner";
import Partner from "@/components/eLearningSchool/Partner";
import Features from "@/components/eLearningSchool/Features";
import AboutUs from "@/components/eLearningSchool/AboutUs";
import PopularCourses from "@/components/eLearningSchool/PopularCourses";
import FeedbackSliderWithFunFacts from "@/components/eLearningSchool/FeedbackSliderWithFunFacts";
import GetInstantCourses from "@/components/eLearningSchool/GetInstantCourses";
import ViewAllCourses from "@/components/eLearningSchool/ViewAllCourses";
import SubscribeForm from "@/components/Common/SubscribeForm";
import Footer from "@/components/_App/Footer";
import baseUrl from "@/utils/baseUrl";
import MembershipFAQ from "@/components/Membership/MembershipFAQ";
import SecurePayment from "@/components/Checkout/SecurePayment";
import BecomeAInstructorBanner from "@/components/BecomeAInstructor/BecomeAnInstructorBanner";
import MembershipMainBanner from "@/components/Membership/MembershipMainBanner";

function Index({ courses, user }) {
	return (
		<>
			<Navbar user={user} />
			<MembershipMainBanner user={user} courses={courses} />
            {/* <SecurePayment />
            <BecomeAInstructorBanner /> */}
			{/* <MembershipFAQ /> */}
			{/* <MainBanner user={user} courses={courses} />
			<Features />
			<PopularCourses user={user} />
			<FeedbackSliderWithFunFacts />
			<GetInstantCourses user={user} />
			<ViewAllCourses />
			<Partner /> */}
			<SubscribeForm />
			<Footer />
		</>
	);
}

// This gets called on every request
// export async function getServerSideProps() {
// 	// Fetch data from external API
// 	// const res = await fetch(`${baseUrl}/api/home-banner`);
// 	// const { courses } = await res.json();

// 	// Pass data to the page via props
// 	// return { props: { courses } };
// }

export default Index;
