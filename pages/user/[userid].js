import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Navbar from "@/components/_App/Navbar";
import Footer from "@/components/_App/Footer";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";

export default function SingleUserPage({user}) {
    const [postList, setPostList] = useState({});
    const router = useRouter();
    const { userid } = router.query;

    useEffect(() => {
        const payload = { params: {userid}};
        const response = axios.get(`${baseUrl}/api/`);
    });

    return(
        <>
            <Navbar user={user}/>
            <div className="col-lg-8 mx-auto p-4 py-md-5">
				
			</div>
            <Footer />
        </>
    );
}