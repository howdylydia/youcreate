import React from "react";
import Navbar from "@/components/_App/Navbar";
import Footer from "@/components/_App/Footer";

export default function CheckoutPage({ user }) {
	
	return (
		<>
			<Navbar user={user} />
			<div className="shadow p-5 m-5 bg-body-tertiary rounded text-center">
				<div><i className="bx bx-check"></i></div>
				<h2>Thank you for your support!</h2>
				<p>You have completed your payment.</p>
				<a href="/posts" className="btn btn-primary">View Posts</a>
			</div>
			<Footer />
		</>
	);
}
