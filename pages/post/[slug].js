import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import PostsDetailsContent from "@/components/SinglePosts/PostsDetailsContent";
import Footer from "@/components/_App/Footer";
import axios from "axios";
import baseUrl from "@/utils/baseUrl";
import toast from "react-hot-toast";

export default function SinglePostsPage({ user }) {
	const [post, setPost] = useState({});
	const router = useRouter();
	const { slug } = router.query;
	
	useEffect(() => {
		const fetchPost = async () => {
			try {
				const payload = {
					params: { slug: slug },
				};
				const url = `${baseUrl}/api/posts/post`;
				const response = await axios.get(url, payload);
				setPost(response.data.post);
				// increment view count
				incrementViewCount(response.data.post.id);
			} catch (err) {
				let message = "An error occurred, but the response structure is unexpected.";
				// Check if err.response and err.response.data exist before destructuring
				if (err.response && err.response.data && err.response.data.message) {
					message = err.response.data.message || err.response.data.error.message;
				}
				toast.error(message, {
					style: {
						border: "1px solid #ff0033",
						padding: "16px",
						color: "#ff0033",
					},
					iconTheme: {
						primary: "#ff0033",
						secondary: "#FFFAEE",
					},
				});
			}
		};

		fetchPost();
	}, [slug]);

	const incrementViewCount = async (postId) => {
		try {
			if (postId) {
				const url = `${baseUrl}/api/posts/post/increment-view-count`;
				const payload = { postId };
				await axios.put(url, payload);
			}
		} catch (error) {
		  console.error("Error incrementing view count:", error);
		}
	};

	return (
		<>
			<Navbar user={user} />

			{/* <PageBanner
				pageTitle={post && post.title}
				homePageUrl="/posts"
				homePageText="Posts"
				activePageText={post && post.title}
			/>
			 */}
			<div className="col-lg-8 mx-auto p-4 py-md-5">
				{post && <PostsDetailsContent user={user} post={post} />}
			</div>
			<Footer />
		</>
	);
}
