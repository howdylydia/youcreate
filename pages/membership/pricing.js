import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import SecurePayment from "@/components/Checkout/SecurePayment";
import SubscribeForm from "@/components/Common/SubscribeForm";
import Footer from "@/components/_App/Footer";
import FeaturedMembership from "@/components/Membership/FeaturedMembership";
import MembershipFAQ from "@/components/Membership/MembershipFAQ";

export default function PricingPage({ user }) {
	return (
		<>
			<Navbar user={user} />

			<PageBanner
				pageTitle="Membership"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Membership"
			/>
			<FeaturedMembership user={user}/>
			<SecurePayment />
			<MembershipFAQ />
			<SubscribeForm />

			<Footer />
		</>
	);
}
