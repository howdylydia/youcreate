import React from "react";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import MembershipContributeForm from "@/components/Membership/MembershipContributeForm";
import Footer from "@/components/_App/Footer";

export default function MembershipContributePage({ user }) {
	return (
		<>
			<Navbar user={user} />

			{/* <PageBanner
				pageTitle="Membership Checkout"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Membership Checkout"
			/> */}
			<MembershipContributeForm user={user} />
			<Footer />
		</>
	);
}
