import React from "react";
import { useEffect } from "react";
import Routher from "next/router";
import Navbar from "@/components/_App/Navbar";
import PageBanner from "@/components/Common/PageBanner";
import Footer from "@/components/_App/Footer";
import MembershipRegisterForm from "@/components/Membership/MembershipRegisterForm";

export default function Register({ user }) {
	useEffect(() => {
		if (user) {
			Routher.push("/membership/contribute");
			return;
		}
		return;
	  }, [user]);

	return (
		<>
			<Navbar user={user} />

			{/* <PageBanner
				pageTitle="Authentication"
				homePageUrl="/"
				homePageText="Home"
				activePageText="Authentication"
			/> */}

			<div className="profile-authentication-area ptb-100">
				<div className="container">
					<div className="row justify-content-center">
						<div className="col-lg-6 col-md-12">
                            {!user && <MembershipRegisterForm />}
						</div>
					</div>
				</div>
			</div>

			<Footer />
		</>
	);
}
